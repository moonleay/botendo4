/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.util

import de.limited_dev.botendo.Bot
import net.dv8tion.jda.api.entities.Activity
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit


object StatusManager {
    lateinit var schedulerService: ScheduledExecutorService

    fun startThread() {
        schedulerService = Executors.newScheduledThreadPool(1)
        schedulerService.scheduleAtFixedRate(
            Runnable {
                val rndm = Random()
                val vals: LinkedList<String?> = object : LinkedList<String?>() {
                    init {
                        add("WAS JETZT CHAT?!")
                        add("Aber nur weil das Spiel scheiße is")
                        add("DANN HALT NICHT")
                        add("Student. 200er IQ")
                        add("Achso, wenn man ihm die Hose auszieht dann hat man was im Mund. Das habe ich nicht bedacht")
                        add("Ich geh jetzt erstmal saufen ey")
                        add("Nintendo what the hell!?")
                        add("FICK DEINE MUTTA")
                        add("Hast du nh' Problem, oder was soll die Scheiße hier?")
                        add("Good Job")
                        add("Businesstendo")
                        add("Will kommen")
                        add("Das zaubert mir doch ein zuckersüßes Lächeln ins Gesicht")
                        add("Ich hab ihn epic Gefesselt und werde ihn demnächst in den See werfen.")
                        add("Ich glaube... nee doch nicht.")
                        add("Willst du mich völlig verarschen??!")
                        add("Ich verlasse jetzt diesen Raum und komme nie wieder zurück")
                        add("Ich bin Gott")
                        add("MEINE FRESSE GEH DA HOCH!!")
                    }
                }
                Bot.jda.presence.activity = Activity.playing("\"" + vals[rndm.nextInt(vals.size)] + "\" ~Domtendo")
            },
            0L,
            60L * 11,
            TimeUnit.SECONDS
        )
    }

}
