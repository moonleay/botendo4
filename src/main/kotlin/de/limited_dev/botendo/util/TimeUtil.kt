/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.util

import java.time.Duration
import java.util.concurrent.TimeUnit

object TimeUtil {
    fun getTimeFormatedShortend(time: Long): String? {
        var time = time
        val days = TimeUnit.MILLISECONDS
            .toDays(time)
        time -= TimeUnit.DAYS.toMillis(days)
        val hours = TimeUnit.MILLISECONDS
            .toHours(time)
        time -= TimeUnit.HOURS.toMillis(hours)
        val minutes = TimeUnit.MILLISECONDS
            .toMinutes(time)
        time -= TimeUnit.MINUTES.toMillis(minutes)
        val seconds = TimeUnit.MILLISECONDS
            .toSeconds(time)
        var s = ""
        if (days >= 1) {
            s += days.toString() + "d "
        }
        if (hours >= 1) {
            s += hours.toString() + "h "
        }
        if (minutes >= 1) {
            s += minutes.toString() + "m "
        }
        if (seconds >= 1 && hours < 1) {
            s += seconds.toString() + "s"
        }
        if (s.isEmpty() || s.isBlank()) {
            s = "0s"
        }
        return s
    }

    fun getTimeFormatedRaw(time: Long): String? {
        var time = time
        val days = TimeUnit.MILLISECONDS
            .toDays(time)
        time -= TimeUnit.DAYS.toMillis(days)
        val hours = TimeUnit.MILLISECONDS
            .toHours(time)
        time -= TimeUnit.HOURS.toMillis(hours)
        val minutes = TimeUnit.MILLISECONDS
            .toMinutes(time)
        time -= TimeUnit.MINUTES.toMillis(minutes)
        val seconds = TimeUnit.MILLISECONDS
            .toSeconds(time)
        var s = ""
        if (days >= 1) {
            s += days.toString() + "d "
        }
        if (hours >= 1) {
            s += hours.toString() + "h "
        }
        if (minutes >= 1) {
            s += minutes.toString() + "m "
        }
        if (seconds >= 1) {
            s += seconds.toString() + "s"
        }
        if (s.isEmpty() || s.isBlank()) {
            s = "0s"
        }
        return s
    }


    //This 100000%ly can be improved, I wrote this at 2am
    fun getTimeUnformated(timeStr: String): Long {
        var days: Long = 0
        var hours: Long = 0
        var minutes: Long = 0
        var seconds: Long = 0
        val timeArr = timeStr.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (s in timeArr) {
            Logger.out(s)
            if (s.endsWith("d")) {
                days = s.split("d".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0].toLong()
            } else if (s.endsWith("h")) {
                hours = s.split("h".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0].toLong()
            } else if (s.endsWith("m")) {
                minutes = s.split("m".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0].toLong()
            } else if (s.endsWith("s")) {
                seconds = s.split("s".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0].toLong()
            }
        }
        Logger.out(
            Duration.ofSeconds(seconds).plus(Duration.ofMinutes(minutes)).plus(Duration.ofHours(hours))
                .plus(Duration.ofDays(days)).toMillis().toString()
        )
        return Duration.ofSeconds(seconds).plus(Duration.ofMinutes(minutes)).plus(Duration.ofHours(hours))
            .plus(Duration.ofDays(days)).toMillis()
    }

}
