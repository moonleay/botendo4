/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.util

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object Logger {
    private val dtf: DateTimeFormatter = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm:ss")

    fun out(msg: String) {
        val caller = Thread.currentThread().stackTrace[2]
        val now: LocalDateTime = LocalDateTime.now()
        try {
            println(
                ("[" + Class.forName(caller.className).simpleName + "." +
                        caller.methodName + ":" + caller.lineNumber + "] [" + dtf.format(now)).toString() + "] <" + msg + ">"
            )
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        }
        // Ich kann nicht mehr
        // [Klasse.Funktion] [T/M HH:MM] <NACHRICHT>
    }

    fun consoleOut(msg: String) {
        val now: LocalDateTime = LocalDateTime.now()
        println(("[console command output] [" + dtf.format(now)) + "] <" + msg + ">")
    }

}
