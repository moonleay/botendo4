/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.util

import java.io.*
import java.util.*


object TokenManager {
    private const val basePath = "./data/"
    private const val filename = "token.nils"
    private const val filePath = basePath + filename
    var token: String? = null
    var dbip: String? = null
    var dbuser: String? = null
    var dbpassword: String? = null

    fun load() {
        val dir = File(basePath)
        if (!dir.exists()) {
            save()
            return
        }
        val configFile = File(dir, filename)
        if (!configFile.exists()) {
            save()
            return
        }
        try {
            val input: InputStream = FileInputStream(filePath)
            val prop = Properties()
            prop.load(input)
            token = if (prop.getProperty("token").equals("empty")) null else prop.getProperty("token")
            input.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun save() {
        val dir = File(basePath)
        if (!dir.exists()) {
            dir.mkdirs()
        }
        val configFile = File(dir, filename)
        if (!configFile.exists()) {
            try {
                configFile.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        try {
            val output: OutputStream = FileOutputStream(filePath)
            val prop = Properties()
            prop.setProperty("token", "emplty")
            prop.store(output, null)
            output.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}
