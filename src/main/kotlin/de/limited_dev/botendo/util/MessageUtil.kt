/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.util

import de.limited_dev.botendo.Bot
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel
import net.dv8tion.jda.api.events.interaction.ModalInteractionEvent
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.events.interaction.command.UserContextInteractionEvent
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent
import org.jetbrains.annotations.Nullable
import java.awt.Color
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit


object MessageUtil {
    private val dtf: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy @ HH:mm:ss")

    fun SendMsg(
        event: SlashCommandInteractionEvent,
        author: String?,
        title: String?,
        color: Color?,
        imageUrl: String?,
        description: String?,
        @Nullable titleOfFlields: Array<String>?,
        @Nullable valueOfFlieds: HashMap<String, Array<String>>,
        inline: Boolean,
        fromMobile: Boolean,
        del: Int
    ) {
        SendMsg(event, author, title, color, imageUrl, description, titleOfFlields, valueOfFlieds, inline, fromMobile)
    }

    fun SendMsg(
        event: SlashCommandInteractionEvent,
        author: String?,
        title: String?,
        color: Color?,
        imageUrl: String?,
        description: String?,
        @Nullable titleOfFlields: Array<String>?,
        @Nullable valueOfFlieds: HashMap<String, Array<String>>,
        inline: Boolean,
        fromMobile: Boolean
    ) {
        SendMsg(event, author, title, color, imageUrl, description, titleOfFlields, valueOfFlieds, inline)
    }


    fun SendMsg(
        event: SlashCommandInteractionEvent,
        author: String?,
        title: String?,
        color: Color?,
        imageUrl: String?,
        description: String?,
        @Nullable titleOfFlields: Array<String>?,
        @Nullable valueOfFlieds: HashMap<String, Array<String>>,
        inline: Boolean
    ) {
        val fromMobile: Boolean
        fromMobile = if (event.getOption("mobileformatting") != null) {
            event.getOption("mobileformatting")!!.asBoolean
        } else {
            false
        }
        val now: LocalDateTime = LocalDateTime.now()
        val eb = EmbedBuilder()
        eb.setAuthor(author)
        eb.setTitle(title)
        eb.setColor(color)
        eb.setDescription(description)
        if (titleOfFlields != null) {
            if (!fromMobile) {
                for (titleOfFlied in titleOfFlields) {
                    var value = ""
                    for (s in valueOfFlieds[titleOfFlied]!!) {
                        value = if (value.isEmpty()) s.toString() else """
     $value
     $s
     """.trimIndent()
                    }
                    eb.addField(titleOfFlied, value, inline)
                }
            } else { // is from mobile
                var tof = ""
                var vof = ""
                for (i in titleOfFlields.indices) {
                    tof += titleOfFlields[i] + if (i == titleOfFlields.size - 1) "" else " -- "
                }
                if (!valueOfFlieds.isEmpty()) {
                    for (i in 0 until valueOfFlieds[titleOfFlields[0]]!!.size) {
                        for (j in titleOfFlields.indices) {
                            vof += valueOfFlieds[titleOfFlields[j]]!![i] + if (j == titleOfFlields.size - 1) "" else " == "
                        }
                        vof += "\n"
                    }
                    eb.addField(tof, vof, inline)
                }
            }
        }
        if (imageUrl != null && !fromMobile) {
            eb.setThumbnail(imageUrl)
        }
        eb.setFooter(">" + dtf.format(now) + " - " + event.user.name + "#" + event.user.discriminator)
        event.hook.sendMessageEmbeds(eb.build()).queue()
    }

    fun sendSimpleOneLiner(event: SlashCommandInteractionEvent, title: String?, description: String?) {
        sendSimpleOneLiner(event, title, description, null)
    }

    fun sendSimpleOneLiner(event: ButtonInteractionEvent, title: String?, description: String?) {
        sendSimpleOneLiner(event, title, description, null)
    }

    fun sendSimpleOneLiner(event: UserContextInteractionEvent, title: String?, description: String?) {
        sendSimpleOneLiner(event, title, description, null)
    }

    fun sendSimpleOneLiner(event: ModalInteractionEvent, title: String?, description: String?) {
        sendSimpleOneLiner(event, title, description, null)
    }

    fun sendSimpleOneLiner(
        event: ModalInteractionEvent,
        title: String?,
        description: String?,
        thumbnailURL: String?
    ) {
        val now: LocalDateTime = LocalDateTime.now()
        val eb = EmbedBuilder()
        eb.setAuthor(Bot.jda.selfUser.name)
        eb.setTitle(title)
        eb.setColor(Color.ORANGE)
        eb.setDescription(description)
        if (thumbnailURL != null) eb.setThumbnail(thumbnailURL)
        eb.setFooter(">" + dtf.format(now) + " - " + event.user.name + "#" + event.user.discriminator)
        event.hook.sendMessageEmbeds(eb.build()).queue()
    }

    fun sendSimpleOneLiner(
        event: SlashCommandInteractionEvent,
        title: String?,
        description: String?,
        thumbnailURL: String?
    ) {
        val now: LocalDateTime = LocalDateTime.now()
        val eb = EmbedBuilder()
        eb.setAuthor(Bot.jda.selfUser.name)
        eb.setTitle(title)
        eb.setColor(Color.ORANGE)
        eb.setDescription(description)
        if (thumbnailURL != null) eb.setThumbnail(thumbnailURL)
        eb.setFooter(">" + dtf.format(now) + " - " + event.user.name + "#" + event.user.discriminator)
        event.hook.sendMessageEmbeds(eb.build()).queue()
    }

    fun sendSimpleOneLiner(
        event: UserContextInteractionEvent,
        title: String?,
        description: String?,
        thumbnailURL: String?
    ) {
        val now: LocalDateTime = LocalDateTime.now()
        val eb = EmbedBuilder()
        eb.setAuthor(Bot.jda.selfUser.name)
        eb.setTitle(title)
        eb.setColor(Color.ORANGE)
        eb.setDescription(description)
        if (thumbnailURL != null) eb.setThumbnail(thumbnailURL)
        eb.setFooter(">" + dtf.format(now) + " - " + event.user.name + "#" + event.user.discriminator)
        if (!event.isAcknowledged) {
            event.replyEmbeds(eb.build()).queue()
            return
        }
        event.hook.sendMessageEmbeds(eb.build()).queue()
    }

    fun sendSimpleOneLiner(
        event: ButtonInteractionEvent,
        title: String?,
        description: String?,
        thumbnailURL: String?
    ) {
        val now: LocalDateTime = LocalDateTime.now()
        val eb = EmbedBuilder()
        eb.setAuthor(Bot.jda.selfUser.name)
        eb.setTitle(title)
        eb.setColor(Color.ORANGE)
        eb.setDescription(description)
        if (thumbnailURL != null) eb.setThumbnail(thumbnailURL)
        eb.setFooter(">" + dtf.format(now) + " - " + event.user.name + "#" + event.user.discriminator)
        event.hook.sendMessageEmbeds(eb.build()).queue()
    }

    fun sendSimpleOneLiner(g: Guild, channelID: Long, title: String?, description: String?) {
        sendSimpleOneLiner(g, channelID, title, description, null)
    }

    fun sendSimpleOneLiner(g: Guild, channelID: Long, title: String?, description: String?, thumbnailURL: String?) {
        val now: LocalDateTime = LocalDateTime.now()
        val eb = EmbedBuilder()
        eb.setAuthor(Bot.jda.selfUser.name)
        eb.setTitle(title)
        eb.setColor(Color.ORANGE)
        eb.setDescription(description)
        if (thumbnailURL != null) eb.setThumbnail(thumbnailURL)
        eb.setFooter(">" + dtf.format(now) + " - Automated Message")
        g.getTextChannelById(channelID)!!.sendMessageEmbeds(eb.build()).queue()
    }

    fun sendSimpleOneLiner(
        g: Guild,
        channelID: Long,
        title: String?,
        description: String?,
        thumbnailURL: String?,
        delay: Int
    ) {
        val now: LocalDateTime = LocalDateTime.now()
        val eb = EmbedBuilder()
        eb.setAuthor(Bot.jda.selfUser.name)
        eb.setTitle(title)
        eb.setColor(Color.ORANGE)
        eb.setDescription(description)
        if (thumbnailURL != null) eb.setThumbnail(thumbnailURL)
        eb.setFooter(">" + dtf.format(now) + " - Automated Message")
        g.getTextChannelById(channelID)!!.sendMessageEmbeds(eb.build()).queueAfter(delay.toLong(), TimeUnit.SECONDS)
    }

    fun getEmbeddedMessageAutomated(
        doAuthor: Boolean,
        title: String?,
        description: String?,
        thumbnailURL: String?,
        shouldAddFooter: Boolean
    ): EmbedBuilder? {
        val now: LocalDateTime = LocalDateTime.now()
        val eb = EmbedBuilder()
        if (doAuthor) eb.setAuthor(Bot.jda.selfUser.name)
        if (title != null) {
            eb.setTitle(title)
        }
        eb.setColor(Color.ORANGE)
        if (description != null) eb.setDescription(description)
        if (thumbnailURL != null) eb.setThumbnail(thumbnailURL)
        if (shouldAddFooter) eb.setFooter(">" + dtf.format(now) + " - Automated Message")
        return eb
    }

    fun getEmbed(title: String?, description: String?): EmbedBuilder {
        return getEmbed(title, description, null)
    }

    fun getEmbedWithAuthor(author: Member?, title: String?, description: String?): EmbedBuilder {
        return getEmbedWithAuthor(author, title, description, null)
    }

    fun getEmbedWithAuthor(author: Member?, title: String?, description: String?, thumbnailURL: String?): EmbedBuilder {
        val now: LocalDateTime = LocalDateTime.now()
        val eb = EmbedBuilder()
        eb.setAuthor(Bot.jda.selfUser.name)
        eb.setTitle(title)
        eb.setColor(Color.ORANGE)
        eb.setDescription(description)
        if (thumbnailURL != null) eb.setThumbnail(thumbnailURL)
        eb.setFooter(">" + dtf.format(now) + " - ${author!!.user.name}#${author.user.discriminator}")
        return eb
    }

    fun getEmbed(title: String?, description: String?, thumbnailURL: String?): EmbedBuilder {
        val now: LocalDateTime = LocalDateTime.now()
        val eb = EmbedBuilder()
        eb.setAuthor(Bot.jda.selfUser.name)
        eb.setTitle(title)
        eb.setColor(Color.ORANGE)
        eb.setDescription(description)
        if (thumbnailURL != null) eb.setThumbnail(thumbnailURL)
        eb.setFooter(">" + dtf.format(now) + " - Automated Message")
        return eb
    }

    // Send message without response handling
    fun sendDMmessage(user: User, content: String) {
        user.openPrivateChannel()
            .flatMap { channel: PrivateChannel ->
                channel.sendMessage(
                    content
                )
            }
            .queue()
    }
}
