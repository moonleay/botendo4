/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.util

import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.interactions.components.buttons.Button

object ButtonUtil {
    fun getButtonsFromRoles(roles: List<Role>): List<Button>? {
        val btns: MutableList<Button> = ArrayList<Button>()
        for (r in roles) {
            btns.add(Button.primary(r.getId(), r.getName()))
        }
        return btns
    }

    fun getMusicControllerButtons(isPaused: Boolean, isRepeating: Boolean): List<Button> {
        val btns: MutableList<Button> = ArrayList<Button>()
        btns.add(Button.secondary("btn:repeat", if (isRepeating) "Stop Repeating" else "Repeat"))
        btns.add(Button.success("btn:pause", if (isPaused) "Continue" else "Pause"))
        btns.add(Button.primary("btn:skip", "Skip Song"))
        btns.add(Button.secondary("btn:queue", "Show Queue"))
        btns.add(Button.danger("btn:stop", "Stop"))
        return btns
    }

    fun getEmptyButtons(): List<Button> {
        val btns: MutableList<Button> = ArrayList<Button>()
        btns.add(Button.danger("btn:null", "EMPTY"))
        return btns
    }
}
