/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.util

import de.limited_dev.botendo.Bot
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.Commands
import net.dv8tion.jda.api.interactions.commands.build.OptionData


object SlashCommandRegister {

    fun registerCommand(){
        Bot.jda.updateCommands().addCommands(
            Commands.slash("info", "Shows Info about the bot")
                .addOptions(OptionData(OptionType.BOOLEAN, "mobileformatting", "Format the reply for mobile devices")),
            Commands.slash("help", "Show a list of all bot commands")
                .addOptions(OptionData(OptionType.BOOLEAN, "mobileformatting", "Format the reply for mobile devices")),
            Commands.slash("buttonroles", "Add Buttons to gain roles when you react to them")
                .addOptions(
                    OptionData(OptionType.ROLE, "role1", "1st Role", true),
                    OptionData(OptionType.ROLE, "role2", "2nd Role"),
                    OptionData(OptionType.ROLE, "role3", "3rd Role"),
                    OptionData(OptionType.ROLE, "role4", "4th Role"),
                    OptionData(OptionType.ROLE, "role5", "5th Role")
                ),
            Commands.slash("play", "Play music with the bot")
                .addOptions(OptionData(OptionType.STRING, "linkquery", "YouTube Link / Search Query", true)),
            Commands.slash("stop", "Stop the music & Bot leaves the voice channel"),
            Commands.slash("nowplaying", "Show what's currently playing"),
            Commands.slash("queue", "Show the music queue"),
            Commands.slash("skip", "Skip the current song"),
            Commands.slash("pause", "Pause the current song"),
            Commands.slash("continue", "Continue the current song, when paused"),
            Commands.slash("repeat", "Toggle repeating the current song"),
            Commands.slash("addplaylist", "Add a playlist")
                .addOptions(OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true))
                .addOptions(OptionData(OptionType.BOOLEAN, "public", "Is this a public or private playlist", true)),
            Commands.slash("listplaylists", "List all playlists")
                .addOptions(OptionData(OptionType.BOOLEAN, "mobileformatting", "Format the reply for mobile devices")),
            Commands.slash("removeplaylist", "Remove a playlist")
                .addOptions(OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true)),
            Commands.slash("addsongtoplaylist", "Add a Song to a playlist")
                .addOptions(OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true))
                .addOptions(OptionData(OptionType.STRING, "youtubelink", "YouTube Link to the desired song", true)),
            Commands.slash("removesongfromplaylist", "Add a Song to a playlist")
                .addOptions(OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true))
                .addOptions(OptionData(OptionType.INTEGER, "index", "Index of wanted song", true)),
            Commands.slash("listallinplaylist", "List all songs in a playlist")
                .addOptions(OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true)),
            Commands.slash("playplaylist", "Play a playlist")
                .addOptions(OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true)),
            Commands.slash("exportplaylist", "Export a playlist")
                .addOptions(OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true)),
            Commands.slash("importplaylist", "Export a playlist")
                .addOptions(OptionData(OptionType.ATTACHMENT, "file", "The database data", true))
                .addOptions(OptionData(OptionType.STRING, "playlistname", "The name of the playlist", true))
                .addOptions(OptionData(OptionType.BOOLEAN, "public", "Is this a public or private playlist", true)),
            Commands.slash("poll", "Create a pol")
                .addOptions(OptionData(OptionType.STRING, "caption", "Caption the poll", true))
                .addOptions(OptionData(OptionType.STRING, "option1", "The 1st option", true))
                .addOptions(OptionData(OptionType.STRING, "option2", "The 2nd option", true))
                .addOptions(OptionData(OptionType.STRING, "option3", "The 3rd option"))
                .addOptions(OptionData(OptionType.STRING, "option4", "The 4th option"))
                .addOptions(OptionData(OptionType.STRING, "option5", "The 5th option"))
                .addOptions(OptionData(OptionType.STRING, "option6", "The 6th option"))
                .addOptions(OptionData(OptionType.STRING, "option7", "The 7th option"))
                .addOptions(OptionData(OptionType.STRING, "option8", "The 8th option"))
                .addOptions(OptionData(OptionType.STRING, "option9", "The 9th option"))
                .addOptions(OptionData(OptionType.STRING, "option10", "The 10th option")),
            Commands.slash("feature", "Change a feature")
                .addOptions(
                    OptionData(OptionType.STRING, "type", "Do you want to add or remove a feature", true)
                        .addChoice("Add", "add")
                        .addChoice("Remove", "remove")
                )
                .addOptions(
                    OptionData(OptionType.STRING, "feature", "The name of the feature", true)
                        .addChoice("Privacy Link Replier", "privacylinkreplier")
                ),
            //Commands.context(Command.Type.USER, "Vote kick")
        ).queue()

    }
}
