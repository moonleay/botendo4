/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.features.privacylink

enum class LinkType(val linkBase: String) {
    TWITTER("https://twitter.com/"),
    REDDIT("https://www.reddit.com/"),
    YOUTUBE("https://www.youtube.com/watch?v="),
    YOUTUBENOWWW("https://youtube.com/watch?v="),
    YOUBE("https://youtu.be/"),
    NONE("none");

    companion object {
        fun getLinkType(msgPrt: String): LinkType {
            for (lt in values()) if (msgPrt.startsWith(lt.linkBase)) return lt
            return NONE
        }
    }
}
