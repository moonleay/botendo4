/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.features.privacylink

import de.limited_dev.botendo.commands.jdacommands.component.JDACommandValues
import de.limited_dev.botendo.util.Logger
import de.limited_dev.botendo.util.MessageUtil.getEmbeddedMessageAutomated
import net.dv8tion.jda.api.events.message.MessageReceivedEvent


object PrivacyLinkFeature {
    private val websiteToFrontend: HashMap<LinkType?, String?> = object : HashMap<LinkType?, String?>() {
        init {
            put(
                LinkType.YOUTUBE,
                "https://piped.kavin.rocks/watch?v="
            ) // www.youtube.com/watch?v= // https://piped.video/watch?v=
            put(LinkType.YOUBE, "https://piped.kavin.rocks/watch?v=") // youtu.be/
            put(LinkType.YOUTUBENOWWW, "https://piped.kavin.rocks/watch?v=") // youtube.com/watch?v=
            put(LinkType.REDDIT, "https://libreddit.kavin.rocks/") // www.reddit.com/
            put(LinkType.TWITTER, "https://nitter.kavin.rocks/") // twitter.com/
        }
    }

    fun handleInput(event: MessageReceivedEvent, msg: String) {
        if (JDACommandValues.DiscordServerPrivacyLink.get(event.guild.idLong) == null || !JDACommandValues.DiscordServerPrivacyLink.get(
                event.guild.idLong
            )!!
        ) return
        val typeList = HashMap<String, LinkType>()
        if (!msg.contains(" ")) {
            val type = LinkType.getLinkType(msg)
            if (type === LinkType.NONE) return
            typeList[msg] = type
        } else {
            for (s in msg.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                val type = LinkType.getLinkType(s)
                if (type === LinkType.NONE) continue
                typeList[s] = type
            }
        }
        val replyBody = StringBuilder()
        for (s in typeList.keys) {
            replyBody.append(">>>").append(getReplyMessage(typeList[s], s)).append("\n")
        }
        if (typeList.isEmpty()) return
        val eb = getEmbeddedMessageAutomated(true, "Private Frontend Link", replyBody.toString(), null, true)
        event.message.replyEmbeds(eb!!.build()).queue()
    }

    fun getMsgOnly(url: String): String? {
        val type = LinkType.getLinkType(url)
        return if (type === LinkType.NONE) "" else getReplyMessage(type, url)
    }

    fun getReplyMessage(type: LinkType?, msg: String): String? {
        return websiteToFrontend[type] + getLinkTail(type, msg)
    }

    fun getLinkTail(type: LinkType?, msg: String): String {
        val returnVal = msg.substring(type!!.linkBase.length, msg.length)
        Logger.out("linkBase: " + type.linkBase + "; msg: " + msg + "; returnval: " + returnVal)
        return returnVal
    }

}
