/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.features.music

import de.limited_dev.botendo.Bot
import de.limited_dev.botendo.features.music.component.Playlist
import de.limited_dev.botendo.util.Logger
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.User


object PlaylistManager {
    private var guildPlaylistMap: MutableMap<Long, MutableList<Playlist>> = mutableMapOf()

    fun loadAllPlaylists() {
        var index = 0
        for (g: Guild in Bot.jda.selfUser.mutualGuilds) {
            Logger.out("Loading Playlists from Guild: " + g.name)
            guildPlaylistMap[g.idLong] =
                (if (PlaylistDataManager.loadGuild(g.idLong) == null) mutableListOf() else PlaylistDataManager.loadGuild(g.idLong)) as MutableList<Playlist>
            index += if (guildPlaylistMap[g.idLong] == null) 0 else guildPlaylistMap[g.idLong]!!.size
        }
        Logger.out("Loaded $index playlists")
    }


    fun getListOfPlaylistsForGuild(g: Guild): List<Playlist?>? {
        return guildPlaylistMap[g.idLong]
    }

    fun getPlaylistFromName(g: Guild, name: String?): Playlist? {
        if(guildPlaylistMap[g.idLong] == null)
            return null
        for (pl in guildPlaylistMap[g.idLong]!!) {
            if (pl.name == name) {
                return pl
            }
        }
        return null
    }

    fun addPlaylist(pl: Playlist): Boolean {
        if (getPlaylistFromName(pl.getGuild(), pl.name) != null) return false
        if(guildPlaylistMap[pl.guildID] == null)
            guildPlaylistMap[pl.guildID] = mutableListOf()
        guildPlaylistMap[pl.guildID]!!.add(pl)
        PlaylistDataManager.savePlaylist(pl)
        return true
    }


    fun removePlaylist(pl: Playlist, u: User): Boolean {
        for (playlist in guildPlaylistMap[pl.guildID]!!) {
            if (playlist.name == pl.name && pl.ownerID == u.idLong) {
                guildPlaylistMap[pl.guildID]!!.remove(playlist)
                PlaylistDataManager.removePlaylist(pl)
                return true
            }
        }
        return false
    }

}
