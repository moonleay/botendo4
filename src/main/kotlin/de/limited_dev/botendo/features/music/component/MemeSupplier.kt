/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.features.music.component


object MemeSupplier {
    private val memeMap: HashMap<MemeType?, List<String?>?> = object : HashMap<MemeType?, List<String?>?>() {
        init {
            put(MemeType.STOP, object : ArrayList<String?>() {
                init {
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1031671269281763438/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032088303895322694/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032088867609792532/unknown.png")
                    add("https://media.discordapp.net/attachments/833442323160891452/1032090259330183188/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032090568047739000/unknown.png")
                }
            })
            put(MemeType.NO_MATCHES, object : ArrayList<String?>() {
                init {
                    add("https://media.discordapp.net/attachments/833442323160891452/1031591718149165128/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032091626228691035/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032092482130944121/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032093080079315005/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032093387450490950/unknown.png")
                }
            })
            put(MemeType.QUEUE, object : ArrayList<String?>() {
                init {
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1031670452642394182/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032100576953126912/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032100945720516658/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032101157633523812/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032101421497208903/unknown.png")
                }
            })
            put(MemeType.EMPTY_QUEUE, object : ArrayList<String?>() {
                init {
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032094190542270535/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032095228426985512/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032096052557398066/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032096577227075604/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032097032736866374/unknown.png")
                }
            })
            put(MemeType.LOAD_FAILED, object : ArrayList<String?>() {
                init {
                    add("https://media.discordapp.net/attachments/833442323160891452/1031656911147372585/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032098383562813491/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032099092907700244/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032099628830707742/unknown.png")
                    add("https://cdn.discordapp.com/attachments/833442323160891452/1032100039234957423/unknown.png")
                }
            })
        }
    }

    private const val min = 1
    private const val max = 5

    fun getMemeLink(type: MemeType): String? {
        return memeMap[type]!![(Math.random() * (max - min + 1) + min).toInt() - 1]
    }

}
