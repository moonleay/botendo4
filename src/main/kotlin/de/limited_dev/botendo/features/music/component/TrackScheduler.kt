/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.features.music.component

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue


class TrackScheduler(val audioPlayer: AudioPlayer) : AudioEventAdapter() {
    var queue: BlockingQueue<AudioTrack>? = null
    var repeating = false


    init{
        queue = LinkedBlockingQueue()
    }

    fun queue(track: AudioTrack) {
        if (!audioPlayer.startTrack(track, true)) {
            queue!!.offer(track)
        }
    }

    fun nextTrack() {
        audioPlayer.startTrack(queue!!.poll(), false)
    }

    override fun onTrackException(player: AudioPlayer?, track: AudioTrack, exception: FriendlyException?) {
        this.audioPlayer.startTrack(track.makeClone(), false)
    }

    override fun onTrackEnd(player: AudioPlayer?, track: AudioTrack, endReason: AudioTrackEndReason) {
        if (endReason.mayStartNext) {
            if (repeating || endReason == AudioTrackEndReason.LOAD_FAILED) {
                this.audioPlayer.startTrack(track.makeClone(), false)
                return
            }
            nextTrack()
        }
    }

    override fun onTrackStuck(player: AudioPlayer?, track: AudioTrack, thresholdMs: Long) {
        this.audioPlayer.startTrack(track.makeClone(), false)
    }

}
