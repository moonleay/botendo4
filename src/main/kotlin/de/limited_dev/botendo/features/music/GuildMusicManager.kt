/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.features.music

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import de.limited_dev.botendo.features.music.component.AudioPlayerSendHandler
import de.limited_dev.botendo.features.music.component.TrackScheduler


class GuildMusicManager(playerManager: AudioPlayerManager) {
    lateinit var audioPlayer: AudioPlayer
    var scheduler: TrackScheduler? = null
    private var sendHandler: AudioPlayerSendHandler? = null

    init{
        audioPlayer = playerManager.createPlayer()
        scheduler = TrackScheduler(audioPlayer)
        audioPlayer.addListener(scheduler)
        sendHandler = AudioPlayerSendHandler(audioPlayer)
    }

    fun getSendHandler(): AudioPlayerSendHandler? {
        return sendHandler
    }

}
