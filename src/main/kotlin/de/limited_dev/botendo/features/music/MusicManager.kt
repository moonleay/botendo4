/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.features.music

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import de.limited_dev.botendo.features.music.component.MemeSupplier
import de.limited_dev.botendo.features.music.component.MemeType
import de.limited_dev.botendo.util.ButtonUtil
import de.limited_dev.botendo.util.MessageUtil.getEmbedWithAuthor
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import de.limited_dev.botendo.util.TimeUtil
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent


object MusicManager {
    private var playerManager: AudioPlayerManager = DefaultAudioPlayerManager()
    private var musicManagerMap: MutableMap<Long, GuildMusicManager>? = null

    init {
        musicManagerMap = HashMap()
        AudioSourceManagers.registerRemoteSources(playerManager)
        AudioSourceManagers.registerLocalSource(playerManager)
    }

    fun getGuildMusicManager(guild: Guild): GuildMusicManager {
        return musicManagerMap!!.computeIfAbsent(guild.idLong) {
            val guildMusicManager: GuildMusicManager = GuildMusicManager(playerManager)
            guild.audioManager.sendingHandler = guildMusicManager.getSendHandler()
            guildMusicManager
        }
    }

    fun loadAndPlay(event: SlashCommandInteractionEvent, trackUrl: String?) {
        loadAndPlay(event, trackUrl, false)
    }

    fun loadAndPlay(event: SlashCommandInteractionEvent, trackUrl: String?, silent: Boolean) {
        val musicManager = getGuildMusicManager(event.guild!!)
        val isPaused = musicManager.scheduler!!.audioPlayer.isPaused
        val isRepeating = musicManager.scheduler!!.repeating
        val m = event.member
        playerManager.loadItemOrdered(musicManager, trackUrl, object : AudioLoadResultHandler {
            override fun trackLoaded(track: AudioTrack) {
                musicManager.scheduler!!.queue(track)
                if (silent) return
                val info = track.info
                val eb = getEmbedWithAuthor(
                    m, "Added to queue from link",
                    ("**" + info.title + "**\n" +
                            "by " + info.author + "\n" +
                            TimeUtil.getTimeFormatedShortend(info.length)).toString() + "\n\n" +
                            ">>>" + info.uri, "https://img.youtube.com/vi/" + getImgURL(info.uri) + "/maxresdefault.jpg"
                )
                event.hook.sendMessageEmbeds(eb.build())
                    .setActionRow(ButtonUtil.getMusicControllerButtons(isPaused, isRepeating))
                    .queue()
            }

            override fun playlistLoaded(playlist: AudioPlaylist) {
                val track = playlist.tracks[0]
                musicManager.scheduler!!.queue(track)
                if (silent) return
                val info = track.info
                val eb = getEmbedWithAuthor(
                    m,
                    "Added to queue from query",
                    (("**" + info.title + "**\n" +
                            "by " + info.author + "\n" +
                            TimeUtil.getTimeFormatedShortend(info.length)).toString() + "\n\n" +
                            ">>>" + info.uri),
                    "https://img.youtube.com/vi/" + getImgURL(info.uri) + "/maxresdefault.jpg"
                )
                event.hook.sendMessageEmbeds(eb.build())
                    .setActionRow(ButtonUtil.getMusicControllerButtons(isPaused, isRepeating))
                    .queue()
            }

            override fun noMatches() {
                if (silent) return
                sendSimpleOneLiner(
                    event,
                    "No Matches",
                    "No Matches found",
                    MemeSupplier.getMemeLink(MemeType.NO_MATCHES)
                )
            }

            override fun loadFailed(exception: FriendlyException) {
                if (silent) return
                sendSimpleOneLiner(
                    event,
                    "Load failed",
                    "Could not load song.",
                    MemeSupplier.getMemeLink(MemeType.LOAD_FAILED)
                )
            }
        })
    }

    private fun getImgURL(uri: String): String {
        return uri.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
    }

}
