/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.features.music.component

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.track.playback.MutableAudioFrame
import net.dv8tion.jda.api.audio.AudioSendHandler
import java.nio.ByteBuffer


class AudioPlayerSendHandler(val audioPlayer: AudioPlayer) : AudioSendHandler {
    private var buffer: ByteBuffer? = null
    private var frame: MutableAudioFrame? = null

    init {
        buffer = ByteBuffer.allocate(1024)
        frame = MutableAudioFrame()
        frame!!.setBuffer(buffer)
    }

    override fun canProvide(): Boolean {
        return audioPlayer!!.provide(frame)
    }

    override fun provide20MsAudio(): ByteBuffer? {
        return buffer!!.flip()
    }

    override fun isOpus(): Boolean {
        return true
    }

}
