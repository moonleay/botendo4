/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.features.music.component

import de.limited_dev.botendo.Bot
import net.dv8tion.jda.api.entities.Guild


class Playlist(val guildID: Long, val ownerID: Long, val name: String, val isPublic: Boolean) {
    private var _public = false

    var linkList: MutableList<String>? = null

    init{
        linkList = ArrayList()
        _public = isPublic
    }

    fun addSong(link: String): Boolean {
        linkList!!.add(link)
        return true
    }

    fun removeSongWithIndex(index: Int): Boolean {
        linkList!!.removeAt(index)
        return true
    }

    fun getGuild(): Guild {
        return Bot.jda.getGuildById(guildID)!!
    }

}
