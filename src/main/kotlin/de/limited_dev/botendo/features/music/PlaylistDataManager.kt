/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.features.music

import de.limited_dev.botendo.Bot
import de.limited_dev.botendo.features.music.component.Playlist
import de.limited_dev.botendo.util.Logger
import java.io.*
import java.util.*


object PlaylistDataManager {
    private var playlistDataManager: PlaylistDataManager? = null
    private const val basePath = "./data/playlists/"

    private fun PlaylistDataManager() {}


    fun loadGuild(guildID: Long): List<Playlist>? {
        val dir = File("$basePath$guildID/")
        if (!dir.exists()) {
            return null
        }
        val playlists: MutableList<Playlist> = ArrayList()
        for (d in dir.listFiles()) {
            if (!d.exists()) {
                Logger.out("File (" + d.name + ") does not exist, this should not happen")
                continue
            }
            try {
                val input: InputStream = FileInputStream(d.absolutePath + "/playlist.metadata")
                val prop = Properties()
                prop.load(input)
                val ownerid = prop.getProperty("ownerid").toLong()
                val guildid = prop.getProperty("guidid").toLong()
                val name = prop.getProperty("name")
                val isPublic = prop.getProperty("ispublic").contains("true")
                input.close()
                Logger.out("Loading Playlist $name")
                val pl = Playlist(guildid, ownerid, name, isPublic)
                if (Bot.jda.getGuildById(guildid)!!.getMemberById(ownerid) == null) {
                    removePlaylist(pl)
                    continue
                }
                val reader = FileReader(d.absolutePath + "/playlist.list")
                val bufferedReader = BufferedReader(reader)
                BufferedReader(reader).use { br ->
                    var line: String?
                    while (br.readLine().also { line = it } != null) {
                        pl.addSong(line!!)
                    }
                }
                playlists.add(pl)
                bufferedReader.close()
                reader.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return playlists
    }


    fun removePlaylist(list: Playlist) {
        val filePath = basePath + "/" + list.guildID + "/" + list.name
        val playlistFolder = File(filePath)
        if (playlistFolder.exists()) {
            Logger.out("Deleted " + playlistFolder.name + " sucessfully")
            deleteFile(playlistFolder)
            return
        }
        Logger.out("The folder " + playlistFolder.name + " does not exist.")
    }

    fun savePlaylist(list: Playlist) {
        val dir = File(basePath + "/" + list.guildID + "/" + list.name)
        if (!dir.exists()) {
            dir.mkdirs()
        }
        savePlaylistMeta(list)
        val filePath = basePath + "/" + list.guildID + "/" + list.name + "/playlist.list"
        val configFile = File(filePath)
        if (configFile.exists()) {
            configFile.delete()
        }
        try {
            configFile.createNewFile()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        try {
            val writer = FileWriter(configFile.absolutePath, true)
            for (s in list.linkList!!) {
                writer.write(s)
                writer.write("\r\n")
            }
            writer.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun savePlaylistMeta(list: Playlist) {
        val filePath = basePath + "/" + list.guildID + "/" + list.name + "/playlist.metadata"
        val f = File(basePath)
        if (!f.exists()) {
            try {
                f.createNewFile()
            } catch (e: IOException) {
                throw RuntimeException(e)
            }
        }
        try {
            val output: OutputStream = FileOutputStream(filePath)
            val prop = Properties()
            prop.setProperty("ownerid", list.ownerID.toString())
            prop.setProperty("guidid", list.guildID.toString())
            prop.setProperty("name", list.name)
            prop.setProperty("ispublic", if (list.isPublic) "true" else "false")
            prop.store(output, null)
            output.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun deleteFile(element: File) {
        if (element.isDirectory) {
            for (sub in element.listFiles()!!) {
                deleteFile(sub)
            }
        }
        element.delete()
    }

}
