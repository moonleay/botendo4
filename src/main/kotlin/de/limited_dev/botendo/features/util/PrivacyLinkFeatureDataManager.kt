/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.features.util

import java.io.*
import java.util.*


object PrivacyLinkFeatureDataManager {
    private const val basePath = "./data/"
    private const val filename = "PrivacyLinkFeature.nils"
    private const val filePath = basePath + filename

    fun load(guilds: List<Long>): HashMap<Long?, Boolean?> {
        val dir = File(basePath)
        if (!dir.exists()) {
            save(HashMap())
            return HashMap()
        }
        val configFile = File(dir, filename)
        if (!configFile.exists()) {
            save(HashMap())
            return HashMap()
        }
        val hm = HashMap<Long?, Boolean?>()
        try {
            val input: InputStream = FileInputStream(filePath)
            val prop = Properties()
            prop.load(input)
            for (l in guilds) {
                if (prop.getProperty(l.toString()) == null) continue
                hm[l] = prop.getProperty(l.toString()) == "true"
            }
            input.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return hm
    }

    fun save(hm: MutableMap<Long?, Boolean?>) {
        val dir = File(basePath)
        if (!dir.exists()) {
            dir.mkdirs()
        }
        val configFile = File(dir, filename)
        if (!configFile.exists()) {
            try {
                configFile.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        try {
            val output: OutputStream = FileOutputStream(filePath)
            val prop = Properties()
            for (l in hm.keys) {
                prop.setProperty(l.toString(), if (hm[l]!!) "true" else "false")
            }
            prop.store(output, null)
            output.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

}
