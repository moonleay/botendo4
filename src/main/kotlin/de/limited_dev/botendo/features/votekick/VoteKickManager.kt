/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.features.votekick

import kotlinx.coroutines.*
import net.dv8tion.jda.api.entities.Member

object VoteKickManager {

    private var user_voters = mutableMapOf<Long, MutableList<Long>>()
    private var user_job = mutableMapOf<Long, Job>()
    private var user_time = mutableMapOf<Long, Long>()
    private var user_votes = mutableMapOf<Long, Int>()
    private var user_reason = mutableMapOf<Long, String>()
    private var user_startingUser = mutableMapOf<Long, Long>()

    fun addVoteForUser(userID: Long, targetId: Long) {
        addVoteForUser(userID, targetId, false, "")
    }

    fun addVoteForUser(userID: Long, targetId: Long, initVote: Boolean, reason: String) {
        if (user_votes[targetId] == null)
            user_votes[targetId] = 0
        if (user_voters[targetId] == null)
            user_voters[targetId] = mutableListOf(userID)
        else
            user_voters[targetId]!!.add(userID)
        if (user_time[targetId] == null) {
            user_time[targetId] = System.currentTimeMillis()
        }
        user_votes[targetId] = user_votes[targetId]!! + 1
        if (initVote) {
            val job = GlobalScope.launch {
                try {
                    delay(5 * 60 * 1000L)
                    println("5 minutes have passed, executing stoping the vote...")
                    removeUserFromKicklist(targetId, true)
                } catch (e: CancellationException) {
                    println("User was kicked and the Job was canceled")
                }
            }
            user_job[targetId] = job
            user_reason[targetId] = reason
            user_startingUser[targetId] = userID
        }
    }

    fun isUserAlreadyInList(targetID: Long): Boolean {
        return user_votes[targetID] != null
    }

    fun removeUserFromKicklist(targetId: Long, alreadyCanceled: Boolean) {
        user_votes.remove(targetId)
        user_voters.remove(targetId)
        user_time.remove(targetId)
        if (!alreadyCanceled && user_job[targetId]!!.isActive)
            user_job[targetId]!!.cancel()
        user_job.remove(targetId)
        user_reason.remove(targetId)
        user_startingUser.remove(targetId)
    }

    fun checkForKick(targetMember: Member): Boolean {
        val tvs = targetMember.voiceState!!
        val amountOfVotesNeeded = (tvs.channel!!.asVoiceChannel().members.size / 2)
        if (user_votes[targetMember.idLong]!! >= amountOfVotesNeeded) {
            removeUserFromKicklist(targetMember.idLong, false)
            return true
        }
        return false
    }

    fun getVotes(targetID: Long): Int {
        return user_votes[targetID] ?: 0
    }

    fun getUsers(targetId: Long): MutableList<Long>? {
        return user_voters[targetId]
    }

    fun getReason(targetId: Long): String? {
        return user_reason[targetId]
    }
}
