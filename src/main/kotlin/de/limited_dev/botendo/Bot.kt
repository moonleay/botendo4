/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo

import de.limited_dev.botendo.build.BuildConstants
import de.limited_dev.botendo.commands.buttoncommands.component.ButtonManager
import de.limited_dev.botendo.commands.consolecommands.component.ConsoleCommandManager
import de.limited_dev.botendo.commands.jdacommands.component.JDACommandManager
import de.limited_dev.botendo.commands.modalcommands.component.ModalCommandManager
import de.limited_dev.botendo.commands.usercontextcommands.component.UserContextCommandManager
import de.limited_dev.botendo.features.music.PlaylistManager
import de.limited_dev.botendo.listener.*
import de.limited_dev.botendo.util.Logger
import de.limited_dev.botendo.util.StatusManager
import de.limited_dev.botendo.util.TokenManager
import kotlinx.coroutines.runBlocking
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.OnlineStatus
import net.dv8tion.jda.api.entities.Activity
import net.dv8tion.jda.api.entities.GuildVoiceState
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.managers.AudioManager
import net.dv8tion.jda.api.requests.GatewayIntent
import net.dv8tion.jda.api.utils.MemberCachePolicy
import net.dv8tion.jda.api.utils.cache.CacheFlag


object Bot {

    lateinit var jda: JDA
    var launchTime: Long = 0
    lateinit var owner: User

    fun start(){
        TokenManager.load()
        launchTime = System.currentTimeMillis()
        jda = JDABuilder.createDefault(TokenManager.token)
            .setActivity(Activity.playing("mit Huebina"))
            .setStatus(OnlineStatus.DO_NOT_DISTURB)
            .enableIntents(GatewayIntent.GUILD_PRESENCES)
            .enableIntents(GatewayIntent.GUILD_MEMBERS)
            .enableIntents(GatewayIntent.MESSAGE_CONTENT)
            .enableIntents(GatewayIntent.GUILD_VOICE_STATES)
            .enableCache(CacheFlag.CLIENT_STATUS)
            .enableCache(CacheFlag.ONLINE_STATUS)
            .setMemberCachePolicy(MemberCachePolicy.ALL)
            .build()

        jda.addEventListener(ReadyListener())
        jda.addEventListener(VoiceChannelListener())
        jda.addEventListener(ChatLinkListener())
        jda.addEventListener(InteractionListener())
        jda.addEventListener(GuildMemberListener())
        jda.awaitReady()
        StatusManager.startThread()

        JDACommandManager.registerCommands()
        ConsoleCommandManager.registerCommands()
        ConsoleCommandManager.registerListener()
        ButtonManager.registerButtons()
        UserContextCommandManager.registerCommands()
        ModalCommandManager.registerCommands()


        if (jda.selfUser.name.contains("Test")) {
            Logger.out("Test mode active.")
            jda.presence.setStatus(OnlineStatus.IDLE)
        }

        PlaylistManager.loadAllPlaylists()

        Logger.out("Adding shutdown hook")

        owner = jda.getUserById(BuildConstants.ownerID)!!

        Runtime.getRuntime().addShutdownHook(object : Thread() {
            override fun run() = runBlocking {
                shutdown()
            }
        })

        Logger.out("Logged into: ${jda.selfUser.name}")
    }

    fun shutdown() {
        Logger.consoleOut("Shutting down...")
        var leftVCs = 0
        var serversChecked = 0
        for (g in jda.selfUser.mutualGuilds) {
            val self: Member = g.selfMember
            val selfState: GuildVoiceState = self.voiceState!!
            ++serversChecked
            Logger.consoleOut("Checking server #$serversChecked \"${g.name}\"...")
            if (selfState.inAudioChannel()) {
                val audioManager: AudioManager = g.audioManager
                Logger.consoleOut("Leaving VC " + audioManager.connectedChannel!!.asVoiceChannel().name + " in " + g.name)
                audioManager.closeAudioConnection()
                ++leftVCs
            } else {
                Logger.consoleOut("VC not connected.")
            }
            Logger.consoleOut("")
        }
        StatusManager.schedulerService.shutdown()
        jda.shutdown()
        Logger.consoleOut("Done!")
        Logger.consoleOut("Had to leave $leftVCs/$serversChecked checked VCs")
        Logger.consoleOut("Qutting...")
    }
}
