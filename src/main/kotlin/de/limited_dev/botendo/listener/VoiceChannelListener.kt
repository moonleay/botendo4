/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.listener

import de.limited_dev.botendo.features.music.GuildMusicManager
import de.limited_dev.botendo.features.music.MusicManager
import de.limited_dev.botendo.features.votekick.VoteKickManager
import de.limited_dev.botendo.util.Logger
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceUpdateEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import net.dv8tion.jda.api.managers.AudioManager


class VoiceChannelListener: ListenerAdapter() {

    override fun onGuildVoiceUpdate(event: GuildVoiceUpdateEvent) {
        val m: Member = event.member
        val g: Guild = event.guild
        val blIsLeft = event.channelJoined == null
        val blIsJoined = event.channelLeft == null
        val blIsMoved = !blIsLeft && !blIsJoined
        if (blIsMoved) {
            Logger.out("User " + m.user.name + " moved to vc " + event.channelJoined!!.name + " in " + g.name + "!")
            return
        } else if (blIsLeft) {
            Logger.out("User " + m.user.name + " left vc " + event.channelLeft!!.name + " in " + g.name + "!")
        } else if (blIsJoined) {
            Logger.out("User " + m.user.name + " joined vc " + event.channelJoined!!.name + " in " + g.name + "!")
        } else {
            Logger.out("Some Error on vc action. This should not happen.")
        }
        if (blIsLeft && m == g.selfMember) {
            val guildMusicManager: GuildMusicManager = MusicManager.getGuildMusicManager(g)
            guildMusicManager.scheduler!!.audioPlayer.stopTrack()
            guildMusicManager.scheduler!!.repeating = false
            guildMusicManager.scheduler!!.queue!!.clear()
            val audioManager: AudioManager = g.audioManager
            audioManager.closeAudioConnection()
            Logger.out("quit selfmember")
        }
        if (blIsLeft && g.selfMember.voiceState!!.inAudioChannel() && g.selfMember.voiceState!!.channel
            !!.asVoiceChannel().members.size == 1
        ) {
            val guildMusicManager: GuildMusicManager = MusicManager.getGuildMusicManager(g)
            guildMusicManager.scheduler!!.audioPlayer.stopTrack()
            guildMusicManager.scheduler!!.queue!!.clear()
            val audioManager: AudioManager = g.audioManager
            audioManager.closeAudioConnection()
            Logger.out("quit selfmember")
        }

        if (VoteKickManager.isUserAlreadyInList(m.idLong)) {
            VoteKickManager.removeUserFromKicklist(m.idLong, false)
        }
    }
}
