/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.listener

import de.limited_dev.botendo.commands.buttoncommands.component.ButtonManager
import de.limited_dev.botendo.commands.jdacommands.component.JDACommandManager
import de.limited_dev.botendo.commands.modalcommands.component.ModalCommandManager
import de.limited_dev.botendo.commands.usercontextcommands.component.UserContextCommandManager
import de.limited_dev.botendo.util.Logger
import de.limited_dev.botendo.util.MessageUtil
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.events.interaction.ModalInteractionEvent
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.events.interaction.command.UserContextInteractionEvent
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter


class InteractionListener : ListenerAdapter() {
    override fun onSlashCommandInteraction(event: SlashCommandInteractionEvent) {
        Logger.out("New Command: /" + event.name + " in " + event.guild!!.name)
        event.deferReply().queue()
        var index = 0
        for (c in JDACommandManager.getCommandsAsObjects()) {
            ++index
            if (event.name == c!!.name) {
                c.onSlashCommand(event)
                return
            }
        }
        MessageUtil.sendSimpleOneLiner(
            event,
            "No Command found",
            "Could not find the right command. This is 110% an error.\nSearched $index commands."
        )
    }


    override fun onUserContextInteraction(event: UserContextInteractionEvent) {
        Logger.out("New UserContextInteraction: /" + event.name + " in " + event.guild!!.name)
        //event.deferReply().queue()
        var index = 0
        for (c in UserContextCommandManager.getCommandsAsObjects()) {
            ++index
            if (event.name == c!!.name) {
                c.onEvent(event)
                return
            }
        }
        MessageUtil.sendSimpleOneLiner(
            event,
            "No Command found",
            "Could not find the right command. This is 110% an error.\nSearched $index commands."
        )
    }

    override fun onModalInteraction(event: ModalInteractionEvent) {
        Logger.out("New ModalInteraction: /" + event.modalId + " in " + event.guild!!.name)
        var index = 0
        for (c in ModalCommandManager.getCommandsAsObjects()) {
            ++index
            if (event.modalId.startsWith(c!!.name)) {
                c.onEvent(event)
                return
            }
        }
        MessageUtil.sendSimpleOneLiner(
            event,
            "No Command found",
            "Could not find the right command. This is 110% an error.\nSearched $index commands."
        )
    }

    override fun onButtonInteraction(event: ButtonInteractionEvent) {
        val g: Guild? = event.guild
        Logger.out("New Button: /" + event.button.label + " in " + event.guild!!.name)
        var index = 0
        for (b in ButtonManager.getButtonsAsObjects()) {
            ++index
            if (event.button.id == "btn:" + b!!.id) {
                Logger.out("Is normal Button; running onClick...")
                b.onClick(event)
                return
            }
        }
        Logger.out("Is not normal button (Searched $index Buttons..); assuming its a role...")
        if (event.message.author.idLong != event.jda.selfUser.idLong) {
            event.deferEdit().queue()
            return
        }
        val r: Role = event.button.id?.let { g?.getRoleById(it) } ?: return
        val m: Member? = event.interaction.member
        Logger.out("Member: " + m!!.user.name + " selected Role: " + r.name)
        if (m.roles.contains(r)) {
            g!!.removeRoleFromMember(m, r).queue()
            event.deferEdit().queue()
            return
        }
        g!!.addRoleToMember(m, r).queue()
        event.deferEdit().queue()
    }
}
