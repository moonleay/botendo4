/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.buttoncommands.component

import de.limited_dev.botendo.commands.buttoncommands.*
import de.limited_dev.botendo.util.Logger
import java.util.concurrent.CopyOnWriteArrayList

object ButtonManager {
    val BUTTONS = CopyOnWriteArrayList<Button>()

    fun getButtonsAsObjects(): Array<Button?> {
        val buttons = arrayOfNulls<Button>(BUTTONS.size)
        for (i in BUTTONS.indices) {
            buttons[i] = BUTTONS[i]
        }
        return buttons
    }

    fun registerButtons() {
        BUTTONS.add(PauseButton())
        BUTTONS.add(QueueButton())
        BUTTONS.add(RepeatButton())
        BUTTONS.add(SkipButton())
        BUTTONS.add(StopButton())
        Logger.out("All Buttons registered")
    }
}
