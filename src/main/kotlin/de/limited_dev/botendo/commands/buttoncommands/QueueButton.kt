/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.buttoncommands

import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo
import de.limited_dev.botendo.commands.buttoncommands.component.Button
import de.limited_dev.botendo.features.music.GuildMusicManager
import de.limited_dev.botendo.features.music.MusicManager
import de.limited_dev.botendo.features.music.component.MemeSupplier
import de.limited_dev.botendo.features.music.component.MemeType
import de.limited_dev.botendo.util.ButtonUtil
import de.limited_dev.botendo.util.MessageUtil
import de.limited_dev.botendo.util.TimeUtil
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent

class QueueButton() : Button("queue") {
    override fun onClick(event: ButtonInteractionEvent) {
        val g: Guild = event!!.guild!!
        val guildMusicManager: GuildMusicManager = MusicManager.getGuildMusicManager(g)
        val queue = guildMusicManager.scheduler!!.queue
        val audioPlayer = guildMusicManager.audioPlayer
        val currenttrack = audioPlayer.playingTrack
        if (queue!!.isEmpty() && currenttrack == null) {
            MessageUtil.sendSimpleOneLiner(
                event,
                "Queue",
                "**Queue Empty**",
                MemeSupplier.getMemeLink(MemeType.EMPTY_QUEUE)
            )
            return
        }
        val trackList: List<AudioTrack> = ArrayList(queue)
        var description = ""
        var info: AudioTrackInfo
        var trackInfo: String
        var index = 0
        info = currenttrack!!.info
        description = """${"**" + info.title + " - " + TimeUtil.getTimeFormatedShortend(info.length)} (${info.author})**
"""
        for (track in trackList) {
            if (index == 14) continue
            info = track.info
            trackInfo = """${info.title + " - " + TimeUtil.getTimeFormatedShortend(track.duration)} (${info.author})
"""
            description += trackInfo
            ++index
        }
        val m = event.member
        val isPaused = guildMusicManager.scheduler!!.audioPlayer.isPaused
        val isRepeating = guildMusicManager.scheduler!!.repeating
        val eb = MessageUtil.getEmbedWithAuthor(m, "Queue", description, MemeSupplier.getMemeLink(MemeType.QUEUE))
        //event.message.editMessageComponents(Collections.emptyList()).queue()
        event.replyEmbeds(eb.build()).setActionRow(ButtonUtil.getMusicControllerButtons(isPaused, isRepeating)).queue()
    }
}
