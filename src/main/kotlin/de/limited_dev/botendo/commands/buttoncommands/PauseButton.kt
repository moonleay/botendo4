/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.buttoncommands

import de.limited_dev.botendo.commands.buttoncommands.component.Button
import de.limited_dev.botendo.features.music.GuildMusicManager
import de.limited_dev.botendo.features.music.MusicManager
import de.limited_dev.botendo.features.music.component.MemeSupplier
import de.limited_dev.botendo.features.music.component.MemeType
import de.limited_dev.botendo.util.ButtonUtil
import de.limited_dev.botendo.util.Logger
import de.limited_dev.botendo.util.MessageUtil
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.GuildVoiceState
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent

class PauseButton() : Button("pause") {
    override fun onClick(event: ButtonInteractionEvent) {
        val g: Guild = event!!.guild!!
        val self: Member = g.selfMember
        val selfState: GuildVoiceState = self.voiceState!!
        if (!selfState.inAudioChannel()) {
            MessageUtil.sendSimpleOneLiner(event, "Not In VC", "I'm not a voice channel", null)
            return
        }
        val m: Member = g.getMemberById(event.user.idLong)!!
        val mState: GuildVoiceState = m.voiceState!!
        if (!mState.inAudioChannel()) {
            MessageUtil.sendSimpleOneLiner(event, "Not in VC", "You are not in a vc", null)
            return
        }
        if (selfState.channel!!.asVoiceChannel() != mState.channel!!.asVoiceChannel()) {
            MessageUtil.sendSimpleOneLiner(event, "Not in same VC", "You are not in my vc", null)
            return
        }
        val guildMusicManager: GuildMusicManager = MusicManager.getGuildMusicManager(g)
        val isRepeating = guildMusicManager.scheduler!!.repeating
        var isPaused = guildMusicManager.scheduler!!.audioPlayer.isPaused
        Logger.out("Editing msg: ${event.message.contentDisplay} from ${event.message.author.name}")
        //event.message.editMessageComponents(Collections.emptyList()).queue()
        if (isPaused) {
            isPaused = false
            guildMusicManager.scheduler!!.audioPlayer.isPaused = isPaused
            val eb = MessageUtil.getEmbedWithAuthor(m, "I'm continuing", "I'm continuing to play the song.")
            event.replyEmbeds(eb.build()).setActionRow(ButtonUtil.getMusicControllerButtons(isPaused, isRepeating))
                .queue()
            return
        }
        isPaused = true
        guildMusicManager.scheduler!!.audioPlayer.isPaused = isPaused
        val eb =
            MessageUtil.getEmbedWithAuthor(m, "I paused", "I paused the song", MemeSupplier.getMemeLink(MemeType.STOP))
        event.replyEmbeds(eb.build()).setActionRow(ButtonUtil.getMusicControllerButtons(isPaused, isRepeating)).queue()
    }
}
