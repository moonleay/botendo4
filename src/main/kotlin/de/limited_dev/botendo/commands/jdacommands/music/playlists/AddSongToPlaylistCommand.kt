/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.music.playlists

import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.features.music.PlaylistDataManager
import de.limited_dev.botendo.features.music.PlaylistManager.getPlaylistFromName
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent


class AddSongToPlaylistCommand: JDACommand("addsongtoplaylist", "Add a song to a playlist", arrayOf("Playlist name", "YouTube-Link")){
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val g: Guild = event!!.guild!!
        val m: Member = event.member!!
        val name = event.getOption("playlistname")!!.asString
        val link = event.getOption("youtubelink")!!.asString
        if (!link.startsWith("https://www.youtube.com") && !link.startsWith("https://youtube.com") && link.startsWith("https://")) {
            sendSimpleOneLiner(event, "Error", "Not a YouTube URL.")
            return
        }
        if (!isURL(link)) {
            sendSimpleOneLiner(event, "Error", "Not a Youtube-link")
            return
        }
        val pl = getPlaylistFromName(g, name)
        if (pl == null) {
            sendSimpleOneLiner(event, "Error", "This playlist does not exist")
            return
        }
        if (!pl.isPublic && pl.ownerID != m.user.idLong) {
            sendSimpleOneLiner(event, "Error", "You don't own this playlist.")
            return
        }
        pl.addSong(link)
        PlaylistDataManager.savePlaylist(pl)
        sendSimpleOneLiner(event, "Link has been added", "You added $link to $name")
    }

    private fun isURL(uri: String): Boolean {
        return uri.startsWith("https://www.youtube.com") || uri.startsWith("https://youtube.com") || uri.startsWith("https://youtu.be")
    }

}
