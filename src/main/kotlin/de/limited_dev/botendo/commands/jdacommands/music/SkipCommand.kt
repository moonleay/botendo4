/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.music

import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.features.music.GuildMusicManager
import de.limited_dev.botendo.features.music.MusicManager
import de.limited_dev.botendo.util.ButtonUtil
import de.limited_dev.botendo.util.MessageUtil.getEmbedWithAuthor
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.GuildVoiceState
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent


class SkipCommand: JDACommand("skip", "Skip the current song") {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val g: Guild = event!!.guild!!
        val self: Member = g.selfMember
        val selfState: GuildVoiceState = self.voiceState!!
        if (!selfState.inAudioChannel()) {
            sendSimpleOneLiner(event, "Not In VC", "I'm not a voice channel")
            return
        }
        val m: Member = g.getMemberById(event.user.idLong)!!
        val mState: GuildVoiceState = m.voiceState!!
        if (!mState.inAudioChannel()) {
            sendSimpleOneLiner(event, "Not in VC", "You are not in a vc")
            return
        }
        if (selfState.channel!!.asVoiceChannel() != mState.channel!!.asVoiceChannel()) {
            sendSimpleOneLiner(event, "Not in same VC", "You are not in my vc")
            return
        }
        val guildMusicManager: GuildMusicManager = MusicManager.getGuildMusicManager(g)
        val audioPlayer = guildMusicManager.audioPlayer
        if (audioPlayer.playingTrack == null) {
            sendSimpleOneLiner(event, "Error", "There is no playing track currently")
            return
        }
        guildMusicManager.scheduler!!.nextTrack()
        val eb = getEmbedWithAuthor(
            m,
            "Skip",
            "Song Skipped",
            "https://cdn.discordapp.com/attachments/833442323160891452/1031670974568988783/unknown.png"
        )
        val isPaused = guildMusicManager.scheduler!!.audioPlayer.isPaused
        val isRepeating = guildMusicManager.scheduler!!.repeating
        event.hook.sendMessageEmbeds(eb.build())
            .setActionRow(ButtonUtil.getMusicControllerButtons(isPaused, isRepeating)).queue()
    }
}
