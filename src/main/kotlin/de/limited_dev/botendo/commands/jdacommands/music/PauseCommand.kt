/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.music

import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.features.music.GuildMusicManager
import de.limited_dev.botendo.features.music.MusicManager
import de.limited_dev.botendo.features.music.component.MemeSupplier.getMemeLink
import de.limited_dev.botendo.features.music.component.MemeType
import de.limited_dev.botendo.util.ButtonUtil
import de.limited_dev.botendo.util.MessageUtil.getEmbedWithAuthor
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.GuildVoiceState
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent


class PauseCommand: JDACommand("pause", "Pause the current song") {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val g: Guild = event!!.guild!!
        val self: Member = g.selfMember
        val selfState: GuildVoiceState = self.voiceState!!
        if (!selfState.inAudioChannel()) {
            sendSimpleOneLiner(event, "Not In VC", "I'm not a voice channel", null)
            return
        }
        val m: Member = g.getMemberById(event.user.idLong)!!
        val mState: GuildVoiceState = m.voiceState!!
        if (!mState.inAudioChannel()) {
            sendSimpleOneLiner(event, "Not in VC", "You are not in a vc", null)
            return
        }
        if (selfState.channel!!.asVoiceChannel() != mState.channel!!.asVoiceChannel()) {
            sendSimpleOneLiner(event, "Not in same VC", "You are not in my vc", null)
            return
        }
        val guildMusicManager: GuildMusicManager = MusicManager.getGuildMusicManager(g)
        if (guildMusicManager.scheduler!!.audioPlayer.isPaused) {
            sendSimpleOneLiner(event, "Already paused", "The Song is already paused.")
            return
        }
        val isPaused = true
        guildMusicManager.scheduler!!.audioPlayer.isPaused = isPaused
        val isRepeating = guildMusicManager.scheduler!!.repeating
        val eb = getEmbedWithAuthor(m, "I paused", "I paused the song", getMemeLink(MemeType.STOP))
        event.hook.sendMessageEmbeds(eb.build())
            .setActionRow(ButtonUtil.getMusicControllerButtons(isPaused, isRepeating)).queue()
    }
}
