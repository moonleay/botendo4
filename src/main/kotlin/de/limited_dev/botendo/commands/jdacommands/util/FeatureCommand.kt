/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.util

import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.commands.jdacommands.component.JDACommandValues
import de.limited_dev.botendo.features.util.PrivacyLinkFeatureDataManager
import de.limited_dev.botendo.util.Logger
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent


class FeatureCommand: JDACommand("feature", "Add or remove a feature", arrayOf("remove/add", "*feature*")) {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val g: Guild = event!!.guild!!
        val m: Member = event.member!!
        if (!m.hasPermission(Permission.ADMINISTRATOR)) {
            sendSimpleOneLiner(event, "Forbidden", "You are not allowed to run this command", null)
            return
        }

        //Add
        if (event.getOption("type")!!.asString == "add") {
            when (event.getOption("feature")!!.asString) {
                "privacylinkreplier" -> {
                    JDACommandValues.DiscordServerPrivacyLink[g.idLong] = true
                    Logger.out("Feature added: privacylinkreplier")
                    sendSimpleOneLiner(event, "Added Privacy Link Replier", "Feature was added")
                    PrivacyLinkFeatureDataManager.save(JDACommandValues.DiscordServerPrivacyLink)
                }

                else -> {}
            }
            return
            //Remove
        } else if (event.getOption("type")!!.asString == "remove") {
            when (event.getOption("feature")!!.asString) {
                "privacylinkreplier" -> {
                    JDACommandValues.DiscordServerPrivacyLink[g.idLong] = false
                    Logger.out("Feature removed: privacylinkreplier")
                    sendSimpleOneLiner(event, "Removed Privacy Link Replier", "Feature was removed")
                    PrivacyLinkFeatureDataManager.save(JDACommandValues.DiscordServerPrivacyLink)
                }

                else -> {}
            }
        }
    }
}
