/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.music.playlists

import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.features.music.PlaylistDataManager
import de.limited_dev.botendo.features.music.PlaylistManager.addPlaylist
import de.limited_dev.botendo.features.music.component.Playlist
import de.limited_dev.botendo.util.Logger
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.IOException


class ImportPlaylistCommand: JDACommand("importplaylist", "Import a playlist", arrayOf("file", "name", "public")) {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val g: Guild = event!!.guild!!
        val m: Member = event.member!!
        val name = event.getOption("playlistname")!!.asString
        val destFldr = File("./data/dwnlds/")
        if (!destFldr.exists()) {
            destFldr.mkdirs()
        }
        val isPublic = event.getOption("public")!!.asBoolean
        val attachment = event.getOption("file")!!.asAttachment
        Logger.out(attachment.fileName)
        val pl = Playlist(g.getIdLong(), m.getIdLong(), name, isPublic)
        if (addPlaylist(pl)) {
            sendSimpleOneLiner(
                event,
                "Added Playlist $name", "This playlist is " + if (isPublic) "Public" else "Private", null
            )
            val time = System.currentTimeMillis()
            val dest = File("./data/dwnlds/" + m.getUser().getIdLong() + "." + time)
            if (dest.exists()) dest.delete()
            try {
                dest.createNewFile()
            } catch (e: IOException) {
                throw RuntimeException(e)
            }
            val future = attachment.proxy.downloadToFile(dest)
            future.exceptionally { error: Throwable ->  // handle possible errors
                error.printStackTrace()
                null
            }
            print("Downloading")
            while (!future.isDone) {
                //wait
                try {
                    Thread.sleep(10)
                } catch (e: InterruptedException) {
                    throw RuntimeException(e)
                }
                print(".")
            }
            print("\n")
            try {
                val reader = FileReader(dest.absolutePath)
                val bufferedReader = BufferedReader(reader)
                var line: String
                while (bufferedReader.readLine().also { line = it } != null && line !== "") {
                    pl.addSong(line)
                }
                bufferedReader.close()
                reader.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            PlaylistDataManager.savePlaylist(pl)
            dest.delete()
            Logger.out("Imported " + name + " in " + g.getName())
            return
        }
        sendSimpleOneLiner(event, "This playlist name is taken", "This playlist's name is taken", null)
    }
}
