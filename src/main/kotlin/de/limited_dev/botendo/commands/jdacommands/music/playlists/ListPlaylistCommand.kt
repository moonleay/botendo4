/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.music.playlists

import de.limited_dev.botendo.Bot
import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.features.music.PlaylistManager.getListOfPlaylistsForGuild
import de.limited_dev.botendo.util.MessageUtil.SendMsg
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import java.awt.Color


class ListPlaylistCommand: JDACommand("listplaylists", "List all playlists") {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val g: Guild = event!!.guild!!
        val playlists = getListOfPlaylistsForGuild(g)
        val values = HashMap<String, Array<String>>()
        val names = mutableListOf<String>()
        val owners = mutableListOf<String>()
        val sizes = mutableListOf<String>()
        val privates = mutableListOf<String>()
        if(playlists != null)
            for (pl in playlists) {
                names.add(pl!!.name)
                owners.add(g.getMemberById(pl.ownerID)!!.asMention)
                sizes.add(pl.linkList!!.size.toString() + "")
                privates.add(if (pl.isPublic) "Yes" else "No")
            }
        values["Name"] = names.toTypedArray()
        values["Owner"] = owners.toTypedArray()
        values["# of Links"] = sizes.toTypedArray()
        values["Is Public?"] = privates.toTypedArray()
        SendMsg(
            event, Bot.jda.selfUser.name, "Playlists", Color.MAGENTA,
            null, "List of playlists from this server", arrayOf("Name", "Owner", "# of Links"), values, true
        )
    }
}
