/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.util

import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.util.MessageUtil.getEmbeddedMessageAutomated
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.emoji.Emoji
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent


class PollCommand: JDACommand("poll", "Create a poll", arrayOf("caption", "option1", "etc")) {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val caption = event!!.getOption("caption")!!.asString
        val voteOptions: MutableList<String> = ArrayList()
        var content = ""
        var index = 0
        for (s in optionNames) {
            val o = event.getOption(s) ?: continue
            voteOptions.add(o.asString)
            content += """${emojiToSelection2[index + 1]} - ${o.asString}
"""
            ++index
        }
        sendSimpleOneLiner(event, "Poll was created", "You can view it below.")
        val eb = getEmbeddedMessageAutomated(
            true, "Poll", """
     $caption
     
     $content
     """.trimIndent(), null, true
        )!!
            .build()
        event.channel.sendMessageEmbeds(eb).queue { message: Message ->
            addReactions(
                message,
                voteOptions
            )
        }
    }

    private fun addReactions(msg: Message, voteOption: List<String>) {
        val thr: Thread = object : Thread() {
            override fun run() {
                for (i in voteOption.indices) {
                    try {
                        sleep(1000)
                    } catch (e: InterruptedException) {
                        throw RuntimeException(e)
                    }
                    msg.addReaction(Emoji.fromUnicode(emojiToSelection1[i + 1]!!)).queue()
                }
                currentThread().interrupt()
            }
        }
        thr.start()
    }

    private val optionNames = listOf(
        "option1",
        "option2",
        "option3",
        "option4",
        "option5",
        "option6",
        "option7",
        "option8",
        "option9",
        "option10"
    )

    private val emojiToSelection1: HashMap<Int?, String?> = object : HashMap<Int?, String?>() {
        init {
            put(1, "1️⃣")
            put(2, "2️⃣")
            put(3, "3️⃣")
            put(4, "4️⃣")
            put(5, "5️⃣")
            put(6, "6️⃣")
            put(7, "7️⃣")
            put(8, "8️⃣")
            put(9, "9️⃣")
            put(10, "🔟")

        }
    }

    private val emojiToSelection2: HashMap<Int?, String?> = object : HashMap<Int?, String?>() {
        init {
            put(1, ":one:")
            put(2, ":two:")
            put(3, ":three:")
            put(4, ":four:")
            put(5, ":five:")
            put(6, ":six:")
            put(7, ":seven:")
            put(8, ":eight:")
            put(9, ":nine:")
            put(10, ":keycap_ten:")
        }
    }

}
