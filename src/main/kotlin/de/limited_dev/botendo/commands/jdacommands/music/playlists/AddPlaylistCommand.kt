/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.music.playlists

import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.features.music.PlaylistManager.addPlaylist
import de.limited_dev.botendo.features.music.component.Playlist
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent


class AddPlaylistCommand: JDACommand("addplaylist", "Add a Playlist", arrayOf("name", "isPublic")) {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val g: Guild = event!!.guild!!
        val m: Member = event.member!!
        val name = event.getOption("playlistname")!!.asString
        val isPublic = event.getOption("public")!!.asBoolean
        val pl = Playlist(g.getIdLong(), m.getIdLong(), name, isPublic)
        if (addPlaylist(pl)) {
            sendSimpleOneLiner(
                event,
                "Added Playlist $name", "This playlist is " + if (isPublic) "Public" else "Private", null
            )
            return
        }
        sendSimpleOneLiner(event, "This playlist name is taken", "This playlist's name is taken", null)
    }
}
