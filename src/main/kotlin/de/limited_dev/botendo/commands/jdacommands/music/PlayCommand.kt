/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.music

import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.features.music.MusicManager
import de.limited_dev.botendo.util.Logger
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.GuildVoiceState
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.managers.AudioManager


class PlayCommand: JDACommand("play", "Play music", arrayOf("YouTubeLink/Search Query")) {

    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val g: Guild = event!!.guild!!
        val self: Member = g.selfMember
        val m: Member = g.getMemberById(event.user.idLong)!!
        val selfState: GuildVoiceState = self.voiceState!!
        val memberState: GuildVoiceState = m.voiceState!!
        var hadToJoin = false
        if (!selfState.inAudioChannel()) {
            if (!attemptToJoinVoice(event, m)) return
            hadToJoin = true
        }
        if (!hadToJoin) {
            if (selfState.channel != memberState.channel) {
                sendSimpleOneLiner(event, "Error", "You are not in the same voice channel", null)
                return
            }
        }
        var link = event.getOption("linkquery")!!.asString
        if (!link.startsWith("https://www.youtube.com") && !link.startsWith("https://youtube.com") && link.startsWith("https://")) {
            sendSimpleOneLiner(event, "Error", "Not a YouTube URL.", null)
            return
        }
        if (!isURL(link)) {
            link = "ytsearch:$link"
            Logger.out("Is not link")
        }
        MusicManager.loadAndPlay(event, link)
    }

    private fun isURL(uri: String): Boolean {
        return uri.startsWith("https://www.youtube.com") || uri.startsWith("https://youtube.com") || uri.startsWith("https://youtu.be")
    }

    private fun attemptToJoinVoice(event: SlashCommandInteractionEvent, m: Member): Boolean {
        val g: Guild = event.guild!!
        val memberVoiceState: GuildVoiceState = m.voiceState!!
        if (!memberVoiceState.inAudioChannel()) {
            sendSimpleOneLiner(
                event, "Not In Voice",
                "You are not in a voice channel.", null
            )
            Logger.out("Could not join vc")
            return false
        }
        val audioManager: AudioManager = g.audioManager
        val vc: net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel = memberVoiceState.channel!!.asVoiceChannel()
        audioManager.openAudioConnection(vc)
        Logger.out("Joined VC in " + g.name)
        return true
    }

}
