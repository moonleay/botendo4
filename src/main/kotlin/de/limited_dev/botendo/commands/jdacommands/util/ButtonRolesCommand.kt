/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.util

import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.util.ButtonUtil
import de.limited_dev.botendo.util.MessageUtil.getEmbeddedMessageAutomated
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent


class ButtonRolesCommand: JDACommand("buttonroles", "", arrayOf("@Role", "etc.")) {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        if (!event!!.member!!.hasPermission(Permission.ADMINISTRATOR)) {
            sendSimpleOneLiner(
                event,
                "You do not have the permission",
                "You have not the permission to use this command",
                null
            )
            return
        }
        val rls: MutableList<Role> = ArrayList<Role>()
        if (event.getOption("role1") != null) {
            rls.add(event.getOption("role1")!!.asRole)
        }
        if (event.getOption("role2") != null) {
            rls.add(event.getOption("role2")!!.asRole)
        }
        if (event.getOption("role3") != null) {
            rls.add(event.getOption("role3")!!.asRole)
        }
        if (event.getOption("role4") != null) {
            rls.add(event.getOption("role4")!!.asRole)
        }
        if (event.getOption("role5") != null) {
            rls.add(event.getOption("role5")!!.asRole)
        }
        if (event.getOption("role6") != null) {
            rls.add(event.getOption("role6")!!.asRole)
        }
        sendSimpleOneLiner(event, "Done", "The Message", null)
        val eb = getEmbeddedMessageAutomated(
            true,
            "Role Selector",
            "Get your role by clicking one of the following.",
            null,
            false
        )
        event.channel.asTextChannel().sendMessageEmbeds(eb!!.build())
            .setActionRow(ButtonUtil.getButtonsFromRoles(rls)!!).queue()
    }
}
