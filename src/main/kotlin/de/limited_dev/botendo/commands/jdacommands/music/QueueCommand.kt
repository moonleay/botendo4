/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.music

import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo
import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.features.music.GuildMusicManager
import de.limited_dev.botendo.features.music.MusicManager
import de.limited_dev.botendo.features.music.component.MemeSupplier.getMemeLink
import de.limited_dev.botendo.features.music.component.MemeType
import de.limited_dev.botendo.util.ButtonUtil
import de.limited_dev.botendo.util.MessageUtil.getEmbedWithAuthor
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import de.limited_dev.botendo.util.TimeUtil
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent


class QueueCommand: JDACommand("queue", "Show the music queue") {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val g: Guild = event!!.guild!!
        val guildMusicManager: GuildMusicManager = MusicManager.getGuildMusicManager(g)
        val queue = guildMusicManager.scheduler!!.queue
        val audioPlayer = guildMusicManager.audioPlayer
        val currenttrack = audioPlayer.playingTrack
        val m = event.member
        if (queue!!.isEmpty() && currenttrack == null) {
            sendSimpleOneLiner(event, "Queue", "**Queue Empty**", getMemeLink(MemeType.EMPTY_QUEUE))
            return
        }
        val trackList: List<AudioTrack> = ArrayList(queue)
        var description = ""
        var info: AudioTrackInfo
        var trackInfo: String
        var index = 0
        info = currenttrack!!.info
        description = """${"**" + info.title + " - " + TimeUtil.getTimeFormatedShortend(info.length)} (${info.author})**
"""
        for (track in trackList) {
            if (index == 14) continue
            info = track.info
            trackInfo = """${info.title + " - " + TimeUtil.getTimeFormatedShortend(track.duration)} (${info.author})
"""
            description += trackInfo
            ++index
        }
        val isPaused = guildMusicManager.scheduler!!.audioPlayer.isPaused
        val isRepeating = guildMusicManager.scheduler!!.repeating
        val eb = getEmbedWithAuthor(m, "Queue", description, getMemeLink(MemeType.QUEUE))
        event.hook.sendMessageEmbeds(eb.build())
            .setActionRow(ButtonUtil.getMusicControllerButtons(isPaused, isRepeating)).queue()
    }
}
