/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.music.playlists

import de.limited_dev.botendo.Bot
import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.features.music.PlaylistManager.getPlaylistFromName
import de.limited_dev.botendo.util.MessageUtil.SendMsg
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import java.awt.Color


class ListAllLinksInPlaylistCommand: JDACommand("listallinplaylist", "List all links in a playlist") {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val g: Guild = event!!.guild!!
        val m: Member = event.member!!
        val pl = getPlaylistFromName(g, event.getOption("playlistname")!!.asString)
        if (pl == null) {
            sendSimpleOneLiner(event, "Error", "This playlist does not exist")
            return
        }
        if (!pl.isPublic && pl.ownerID != m.user.idLong) {
            sendSimpleOneLiner(event, "Error", "You don't own this playlist.")
            return
        }
        val values = HashMap<String, Array<String>>()
        val indexes = mutableListOf<String>()
        val links = mutableListOf<String>()
        var index = 1
        for (s in pl.linkList!!) {
            indexes.add(index.toString() + "")
            links.add(s)
            ++index
        }
        values["Index"] = indexes.toTypedArray()
        values["Link"] = links.toTypedArray()
        SendMsg(
            event, Bot.jda.selfUser.name, "Playlist \"" + pl.name + "\"", Color.MAGENTA,
            null, "List of Links saved in playlist " + pl.name, arrayOf("Index", "Link"), values, true
        )
    }
}
