/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.music.playlists

import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.features.music.GuildMusicManager
import de.limited_dev.botendo.features.music.MusicManager
import de.limited_dev.botendo.features.music.PlaylistManager.getPlaylistFromName
import de.limited_dev.botendo.util.ButtonUtil
import de.limited_dev.botendo.util.Logger
import de.limited_dev.botendo.util.MessageUtil.getEmbedWithAuthor
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.GuildVoiceState
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.managers.AudioManager


class PlayPlaylistCommand: JDACommand("playplaylist", "Play a playlist", arrayOf("playlistname")) {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val g: Guild = event!!.guild!!
        val self: Member = g.selfMember
        val m: Member = g.getMemberById(event.user.idLong)!!
        val selfState: GuildVoiceState = self.voiceState!!
        val memberState: GuildVoiceState = m.voiceState!!
        var hadToJoin = false
        if (!selfState.inAudioChannel()) {
            if (!attemptToJoinVoice(event, m)) return
            hadToJoin = true
        }
        if (!hadToJoin) {
            if (selfState.channel != memberState.channel) {
                sendSimpleOneLiner(event, "Error", "You are not in the same voice channel", null)
                return
            }
        }
        val pl = getPlaylistFromName(g, event.getOption("playlistname")!!.asString)
        if (!pl!!.isPublic && pl.ownerID != m.user.idLong) {
            sendSimpleOneLiner(event, "Error", "You don't own this playlist.")
            return
        }
        for (s in pl.linkList!!) {
            MusicManager.loadAndPlay(event, s, true)
        }

        val eb = getEmbedWithAuthor(m, "Now Playing", "The \"" + pl.name + "\" list!")
        val guildMusicManager: GuildMusicManager = MusicManager.getGuildMusicManager(g)
        val isPaused = guildMusicManager.scheduler!!.audioPlayer.isPaused
        val isRepeating = guildMusicManager.scheduler!!.repeating
        event.hook.sendMessageEmbeds(eb.build())
            .setActionRow(ButtonUtil.getMusicControllerButtons(isPaused, isRepeating)).queue()
    }

    fun attemptToJoinVoice(event: SlashCommandInteractionEvent, m: Member): Boolean {
        val g: Guild = event.guild!!
        val memberVoiceState: GuildVoiceState = m.voiceState!!
        if (!memberVoiceState.inAudioChannel()) {
            sendSimpleOneLiner(
                event, "Not In Voice",
                "You are not in a voice channel.", null
            )
            Logger.out("Could not join vc")
            return false
        }
        val audioManager: AudioManager = g.audioManager
        val vc: VoiceChannel = memberVoiceState.channel!!.asVoiceChannel()
        audioManager.openAudioConnection(vc)
        Logger.out("Joined VC in " + g.name)
        return true
    }

}
