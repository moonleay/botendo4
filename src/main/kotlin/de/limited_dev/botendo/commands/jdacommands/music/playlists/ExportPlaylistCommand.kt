/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.music.playlists

import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.features.music.PlaylistManager.getPlaylistFromName
import de.limited_dev.botendo.util.MessageUtil.getEmbeddedMessageAutomated
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.utils.FileUpload
import java.io.File


class ExportPlaylistCommand: JDACommand("exportplaylist", "Export a playlist", arrayOf("playlist name")) {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val g: Guild = event!!.guild!!
        val m: Member = event.member!!
        val pl = getPlaylistFromName(g, event.getOption("playlistname")!!.asString)
        if (pl == null) {
            sendSimpleOneLiner(event, "Error", "This playlist does not exist")
            return
        }
        if (!pl.isPublic && pl.ownerID != m.user.idLong) {
            sendSimpleOneLiner(event, "Error", "You don't own this playlist.")
            return
        }
        val db = File("./data/playlists/" + g.getIdLong() + "/" + pl.name + "/playlist.list")
        if (!db.exists()) {
            sendSimpleOneLiner(event, "Error", "We could not find the file you were looking for.")
            return
        }
        val eb = getEmbeddedMessageAutomated(
            true,
            pl.name + " playlist",
            "Above is the data for the playlist.",
            null,
            true
        )
        m.getUser().openPrivateChannel().submit()
            .thenCompose { channel ->
                channel.sendMessageEmbeds(eb!!.build()).addFiles(FileUpload.fromData(db)).submit()
            }
            .whenComplete { message, error ->
                if (error != null) sendSimpleOneLiner(
                    event,
                    "Error",
                    "The playlist could not be send to you, because you have PMs disabled.\nEnable PMs and try again."
                ) else sendSimpleOneLiner(event, "Success", "Sent playlist data to you in your DMs.")
            }
    }
}
