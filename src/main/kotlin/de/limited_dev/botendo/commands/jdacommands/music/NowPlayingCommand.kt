/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.music

import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.features.music.GuildMusicManager
import de.limited_dev.botendo.features.music.MusicManager
import de.limited_dev.botendo.util.ButtonUtil
import de.limited_dev.botendo.util.MessageUtil.getEmbedWithAuthor
import de.limited_dev.botendo.util.MessageUtil.sendSimpleOneLiner
import de.limited_dev.botendo.util.TimeUtil
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.GuildVoiceState
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent


class NowPlayingCommand: JDACommand("nowplaying", "Show what's currently playing") {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val g: Guild = event!!.guild!!
        val m = event.member
        val self: Member = g.selfMember
        val selfState: GuildVoiceState = self.voiceState!!
        if (!selfState.inAudioChannel()) {
            sendSimpleOneLiner(event, "Not in VC", "I'm not playing anything.", null)
            return
        }
        val guildMusicManager: GuildMusicManager = MusicManager.getGuildMusicManager(g)
        val audioPlayer = guildMusicManager.audioPlayer
        val track = audioPlayer.playingTrack
        if (track == null) {
            sendSimpleOneLiner(event, "Not Playing", "Not playing anything", null)
            return
        }
        val info = track.info
        val eb = getEmbedWithAuthor(
            m, "Currently playing",
            "**" + info.title + "**\n" +
                    "by " + info.author + "\n" +
                    TimeUtil.getTimeFormatedShortend(audioPlayer.playingTrack.position) + " : " + TimeUtil.getTimeFormatedShortend(
                info.length
            ) + "\n\n" +
                    ">>>" + info.uri, "https://img.youtube.com/vi/" + getImgURL(info.uri) + "/maxresdefault.jpg"
        )
        val isPaused = guildMusicManager.scheduler!!.audioPlayer.isPaused
        val isRepeating = guildMusicManager.scheduler!!.repeating
        event.hook.sendMessageEmbeds(eb.build())
            .setActionRow(ButtonUtil.getMusicControllerButtons(isPaused, isRepeating)).queue()
    }

    private fun getImgURL(uri: String): String {
        return uri.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
    }

}
