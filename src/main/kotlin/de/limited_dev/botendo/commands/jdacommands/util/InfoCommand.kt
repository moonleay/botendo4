/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.util

import de.limited_dev.botendo.Bot
import de.limited_dev.botendo.build.BuildConstants
import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.commands.jdacommands.component.JDACommandManager.getCommandsAsObjects
import de.limited_dev.botendo.util.MessageUtil.SendMsg
import de.limited_dev.botendo.util.TimeUtil
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import java.awt.Color


class InfoCommand: JDACommand("info", "Shows infos about the bot") {

    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val hm = HashMap<String, Array<String>>()
        hm["Information"] =
            arrayOf("Version", "Ping", "Uptime", "RAM", "# Commands")
        hm["Value"] =
            arrayOf(
                BuildConstants.botVersion,
                Bot.jda.gatewayPing.toString(),
                TimeUtil.getTimeFormatedShortend(System.currentTimeMillis() - Bot.launchTime).toString(),
                ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime()
                    .freeMemory()) / 1024 / 1024).toString() + "MB / " + (Runtime.getRuntime()
                    .maxMemory() - (Runtime.getRuntime().totalMemory() - Runtime.getRuntime()
                    .freeMemory())) / 1024 / 1024 + "MB; " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime()
                    .freeMemory()) * 100L / Runtime.getRuntime().maxMemory() + "%",
                getCommandsAsObjects().size.toString()
            )
        SendMsg(
            event!!,
            Bot.jda.selfUser.name + " v." + BuildConstants.botVersion,
            "Bot information",
            Color.YELLOW,
            Bot.jda.selfUser.avatarUrl,
            "General information about the bot",
            arrayOf("Information", "Value"),
            hm,
            true
        )
    }

}
