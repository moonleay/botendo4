/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.util

import de.limited_dev.botendo.commands.jdacommands.component.JDACommand
import de.limited_dev.botendo.commands.jdacommands.component.JDACommandManager.getCommandsAsObjects
import de.limited_dev.botendo.util.MessageUtil.SendMsg
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import java.awt.Color


class HelpCommand: JDACommand("help", "List all commands") {
    override fun onSlashCommand(event: SlashCommandInteractionEvent?) {
        val values = mutableMapOf<String, Array<String>>()
        val arguments = mutableListOf<String>()
        val disc = mutableListOf<String>()
        for (c in getCommandsAsObjects()) {
            var commandArgs = ""
            if (c!!.options == null) {
                commandArgs = c.name
                arguments.add(commandArgs)
            } else {
                for (ca in c.options!!) {
                    commandArgs = if (commandArgs.isEmpty()) "<$ca>" else "$commandArgs <$ca>"
                }
                commandArgs = c.name + " " + commandArgs
                arguments.add(commandArgs)
            }
            disc.add(if (c.description.isEmpty()) "No description" else c.description)
        }
        values["Command"] = arguments.toTypedArray()
        values["Description"] = disc.toTypedArray()
        SendMsg(
            event!!, "Uses / commands", "Commands", Color.MAGENTA,
            null, "List of avalible commands", arrayOf("Command", "Description"), values as HashMap<String, Array<String>>, true, true, 0
        )
    }

}
