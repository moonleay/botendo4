/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.jdacommands.component

import de.limited_dev.botendo.Bot
import de.limited_dev.botendo.commands.jdacommands.music.*
import de.limited_dev.botendo.commands.jdacommands.music.playlists.*
import de.limited_dev.botendo.commands.jdacommands.util.*
import de.limited_dev.botendo.features.util.PrivacyLinkFeatureDataManager
import de.limited_dev.botendo.util.Logger
import java.util.concurrent.CopyOnWriteArrayList


object JDACommandManager {
    val JDA_COMMANDS = CopyOnWriteArrayList<JDACommand>()

    fun getCommandsAsObjects(): Array<JDACommand?> {
        val commands = arrayOfNulls<JDACommand>(JDA_COMMANDS.size)
        for (i in JDA_COMMANDS.indices) {
            commands[i] = JDA_COMMANDS[i]
        }
        return commands
    }

    fun registerCommands() {
        JDA_COMMANDS.add(InfoCommand())
        JDA_COMMANDS.add(HelpCommand())
        JDA_COMMANDS.add(ButtonRolesCommand())
        JDA_COMMANDS.add(FeatureCommand())
        JDA_COMMANDS.add(PollCommand())
        JDA_COMMANDS.add(PlayCommand())
        JDA_COMMANDS.add(StopCommand())
        JDA_COMMANDS.add(NowPlayingCommand())
        JDA_COMMANDS.add(QueueCommand())
        JDA_COMMANDS.add(SkipCommand())
        JDA_COMMANDS.add(PauseCommand())
        JDA_COMMANDS.add(ContinueCommand())
        JDA_COMMANDS.add(RepeatCommand())
        JDA_COMMANDS.add(AddPlaylistCommand())
        JDA_COMMANDS.add(ListPlaylistCommand())
        JDA_COMMANDS.add(RemovePlaylistCommand())
        JDA_COMMANDS.add(AddSongToPlaylistCommand())
        JDA_COMMANDS.add(ListAllLinksInPlaylistCommand())
        JDA_COMMANDS.add(RemoveSongFromPlaylistCommand())
        JDA_COMMANDS.add(PlayPlaylistCommand())
        JDA_COMMANDS.add(ExportPlaylistCommand())
        JDA_COMMANDS.add(ImportPlaylistCommand())
        Logger.out("All Commands registered")

        val lList: MutableList<Long> = ArrayList()
        for (g in Bot.jda.selfUser.mutualGuilds) {

            lList.add(g.idLong)
        }
        JDACommandValues.DiscordServerPrivacyLink = PrivacyLinkFeatureDataManager.load(lList)
    }
}
