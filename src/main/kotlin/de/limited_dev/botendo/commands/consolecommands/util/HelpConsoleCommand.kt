/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.consolecommands.util

import de.limited_dev.botendo.commands.consolecommands.component.ConsoleCommand
import de.limited_dev.botendo.commands.consolecommands.component.ConsoleCommandManager.getCommands
import de.limited_dev.botendo.util.Logger


class HelpConsoleCommand : ConsoleCommand("help", "Shows all commands") {
    override fun onCommand(args: Array<String?>) {
        var out = "List of available console commands:\n"
        val commandList = getCommands()
        for (i in commandList!!.indices) {
            out = out + commandList[i].name + if (i == commandList.size - 1) "" else ","
        }
        Logger.consoleOut(out)
    }
}
