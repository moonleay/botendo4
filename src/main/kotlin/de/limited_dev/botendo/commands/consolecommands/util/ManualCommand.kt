/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.consolecommands.util

import de.limited_dev.botendo.commands.consolecommands.component.ConsoleCommand
import de.limited_dev.botendo.commands.consolecommands.component.ConsoleCommandManager.sendUsage
import java.util.*


class ManualCommand :
    ConsoleCommand("man", "Shows the correct usage of a command & its description", arrayOf("command name")) {

    override fun onCommand(args: Array<String?>) {
        if (args.size != 1) {
            sendUsage(name)
            return
        }
        val commandName = args[0]
        sendUsage(commandName!!.lowercase(Locale.getDefault()))
    }
}
