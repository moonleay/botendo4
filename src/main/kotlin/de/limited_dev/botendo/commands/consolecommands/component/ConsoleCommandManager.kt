/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.consolecommands.component

import de.limited_dev.botendo.commands.consolecommands.system.BotVersionCommand
import de.limited_dev.botendo.commands.consolecommands.system.ShutdownCommand
import de.limited_dev.botendo.commands.consolecommands.util.HelpConsoleCommand
import de.limited_dev.botendo.commands.consolecommands.util.ManualCommand
import de.limited_dev.botendo.util.Logger
import java.util.*


object ConsoleCommandManager {
    private val commands: MutableList<ConsoleCommand> = mutableListOf()

    fun registerCommands() {
        commands.add(ShutdownCommand())
        commands.add(HelpConsoleCommand())
        commands.add(ManualCommand())
        commands.add(BotVersionCommand())
    }

    fun registerListener() {
        val thr: Thread = object : Thread() {
            override fun run() {
                while (true) {
                    if (handleConsoleInput()) {
                        break
                    }
                }
            }
        }
        thr.start()
    }

    fun sendUsage(commandName: String) {
        val command = getCommand(commandName)
        if (command == null) {
            sendUsage("man")
            return
        }
        var out = "Command: " + command.name + " = " + command.description + ""
        Logger.consoleOut(out)
        out = "Usage: $commandName"
        if (command.arguments == null) {
            Logger.consoleOut(out)
            return
        }
        out += if (command.arguments.isEmpty()) "" else " "
        for (i in 0 until command.arguments.size) {
            out = out + "<" + command.arguments[i] + ">" + if (command.arguments.size - 1 == i) "" else " "
        }
        Logger.consoleOut(out)
    }

    fun handleConsoleInput(): Boolean {
        val `in` = Scanner(System.`in`)
        val input: String = `in`.nextLine()
        Logger.out("Console input: $input")
        val commandArr = getCommandArray(input)
        val command = commandArr[0]
        val args = getArgumentsFromCommandArray(commandArr)
        for (c in commands) {
            if (c.name == command) {
                c.onCommand(args)
            }
        }
        return if ("shutdown" == command) {
            true
        } else false
    }

    private fun getCommandArray(command: String): Array<String> {
        return command.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    }

    private fun getArgumentsFromCommandArray(commandArr: Array<String>): Array<String?> {
        val args = arrayOfNulls<String>(commandArr.size - 1)
        var i = 1
        var j = 0
        while (i < commandArr.size) {
            args[j++] = commandArr[i]
            i++
        }
        return args
    }

    fun getCommand(name: String?): ConsoleCommand? {
        for (c in commands) {
            if (c.name == name) return c
        }
        return null
    }

    fun getCommands(): List<ConsoleCommand>? {
        return commands
    }

}
