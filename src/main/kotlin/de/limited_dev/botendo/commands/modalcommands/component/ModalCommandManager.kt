/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.modalcommands.component

import de.limited_dev.botendo.commands.modalcommands.VoteKickModal
import de.limited_dev.botendo.util.Logger
import java.util.concurrent.CopyOnWriteArrayList

object ModalCommandManager {
    val MODAL_COMMANDS = CopyOnWriteArrayList<ModalCommand>()

    fun getCommandsAsObjects(): Array<ModalCommand?> {
        val commands = arrayOfNulls<ModalCommand>(MODAL_COMMANDS.size)
        for (i in MODAL_COMMANDS.indices) {
            commands[i] = MODAL_COMMANDS[i]
        }
        return commands
    }

    fun registerCommands() {
        MODAL_COMMANDS.add(VoteKickModal())
        Logger.out("All ModalCommands registered")
    }
}
