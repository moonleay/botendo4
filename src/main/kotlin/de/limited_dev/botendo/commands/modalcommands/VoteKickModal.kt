/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.modalcommands

import de.limited_dev.botendo.commands.modalcommands.component.ModalCommand
import de.limited_dev.botendo.features.votekick.VoteKickManager
import de.limited_dev.botendo.util.Logger
import de.limited_dev.botendo.util.MessageUtil
import net.dv8tion.jda.api.events.interaction.ModalInteractionEvent

class VoteKickModal : ModalCommand("votekick") {
    override fun onEvent(event: ModalInteractionEvent) {
        Logger.out("votekick sent")
        val targetUserID = event.modalId.split(":")[1]
        val t = event.guild!!.getMemberById(targetUserID)!!
        val tu = t.user
        val tvs = t.voiceState!!
        if (!tvs.inAudioChannel()) {
            val reeb = MessageUtil.getEmbed("Nope", "This user is not in a voice channel.").build()
            event.replyEmbeds(reeb).queue()
            return
        }
        if (!tvs.channel!!.asVoiceChannel().members.contains(event.member)) {
            MessageUtil.sendSimpleOneLiner(event, "Nope", "You are not in the voice channel")
            return
        }
        val reason = event.getValue("reason")!!.asString
        VoteKickManager.addVoteForUser(event.user.idLong, tu.idLong, true, reason)
        val eb = MessageUtil.getEmbed(
            "Vote kick ${tu.name}#${tu.discriminator}",
            "Votes: **1/${tvs.channel!!.asVoiceChannel().members.size / 2 + 1}**\nFrom: ${event.user.name}#${event.user.discriminator}\nReason: ${reason}"
        ).build()
        event.replyEmbeds(eb).queue()

        val shouldKick = VoteKickManager.checkForKick(t)
        if (shouldKick) {
            event.guild!!.kickVoiceMember(t).queue()
            MessageUtil.sendSimpleOneLiner(
                event.guild!!,
                event.channel.idLong,
                "Kicked ${tu.name}#${tu.discriminator}",
                "From: ${event.user.name}#${event.user.discriminator}\nReason: $reason"
            )
        }
    }
}
