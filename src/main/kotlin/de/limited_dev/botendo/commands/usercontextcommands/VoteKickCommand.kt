/*
 *     BotendoKotlin
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.botendo.commands.usercontextcommands

import de.limited_dev.botendo.build.BuildConstants
import de.limited_dev.botendo.commands.usercontextcommands.component.UserContextCommand
import de.limited_dev.botendo.features.votekick.VoteKickManager
import de.limited_dev.botendo.util.MessageUtil
import net.dv8tion.jda.api.events.interaction.command.UserContextInteractionEvent
import net.dv8tion.jda.api.interactions.components.text.TextInput
import net.dv8tion.jda.api.interactions.components.text.TextInputStyle
import net.dv8tion.jda.api.interactions.modals.Modal

class VoteKickCommand : UserContextCommand("Vote kick", "Votekick a user") {

    override fun onEvent(event: UserContextInteractionEvent) {
        if (event.targetMember == event.guild!!.selfMember) {
            MessageUtil.sendSimpleOneLiner(event, "Nope", "You may not kick myself!")
            return
        }

        if (event.target.id == BuildConstants.ownerID) {
            MessageUtil.sendSimpleOneLiner(event, "Nope", "You may not kick my creator!")
            return
        }
        val t = event.guild!!.getMemberById(event.target.idLong)!!
        val tu = t.user
        val tvs = t.voiceState!!

        if (!tvs.channel!!.asVoiceChannel().members.contains(event.member)) {
            MessageUtil.sendSimpleOneLiner(event, "Nope", "You are not in the voice channel")
            return
        }

        if (!VoteKickManager.isUserAlreadyInList(event.target.idLong)) {
            val body: TextInput = TextInput.create("reason", "Add a Reason", TextInputStyle.SHORT)
                .setPlaceholder("Explain why ${event.target.name}#${event.target.discriminator} should be kicked")
                .setMaxLength(150)
                .build()

            val modal: Modal = Modal.create(
                "votekick:${event.target.id}",
                "Vote Kick: ${event.user.name}#${event.user.discriminator}?"
            )
                .addActionRow(body)
                .build()
            event.replyModal(modal).queue()
            return
        }
        if (!tvs.inAudioChannel()) {
            val reeb = MessageUtil.getEmbed("Nope", "This user is not in a voice channel.").build()
            event.replyEmbeds(reeb).queue()
        }
        VoteKickManager.addVoteForUser(event.user.idLong, event.target.idLong)
        val voters = VoteKickManager.getUsers(event.target.idLong)!!
        var listOfnames = ""
        for (l in voters) {
            val u = event.guild!!.getMemberById(l)!!
            listOfnames += "${u.user.name}#${u.user.discriminator}\n"
        }
        val reason = VoteKickManager.getReason(t.idLong)!!
        val eb = MessageUtil.getEmbed(
            "Vote kick ${tu.name}#${tu.discriminator}",
            "Votes: **${VoteKickManager.getVotes(t.idLong)}/${tvs.channel!!.asVoiceChannel().members.size / 2 + 1}**\nFrom: $listOfnames Reason: $reason"
        ).build()
        event.replyEmbeds(eb).queue()


        val shouldKick = VoteKickManager.checkForKick(t)
        if (shouldKick) {
            event.guild!!.kickVoiceMember(t).queue()
            MessageUtil.sendSimpleOneLiner(
                event.guild!!,
                event.channel!!.idLong,
                "Kicked ${tu.name}#${tu.discriminator}",
                "From: $voters Reason: $reason"
            )
            t.user.openPrivateChannel().submit()
                .whenComplete { message, error ->
                    MessageUtil.sendSimpleOneLiner(
                        event,
                        "Vote kick",
                        "You have been removed from your voice channel, because you have been vote kicked.\n" +
                                "**Reason: ${reason}**\nVoters:\n$listOfnames"
                    )
                }
        }
    }
}
