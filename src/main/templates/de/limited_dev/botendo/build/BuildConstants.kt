package de.limited_dev.botendo.build

internal object BuildConstants {
    const val botVersion = "${version}"
    const val ownerID = "${ownerID}"
}
