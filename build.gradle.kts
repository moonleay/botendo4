import org.jetbrains.gradle.ext.ProjectSettings
import org.jetbrains.gradle.ext.TaskTriggersConfig
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.21"
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("org.jetbrains.gradle.plugin.idea-ext") version "1.1.6"
    `maven-publish`
}

val ownerID = 372703841151614976L
group = "de.limited_dev.botendo"
version = System.getenv("CI_COMMIT_TAG")?.let { "$it-${System.getenv("CI_COMMIT_SHORT_SHA")}-prod" }
    ?: System.getenv("CI_COMMIT_SHORT_SHA")?.let { "$it-dev" }
            ?: "DevelopmentBuild"

val mavenArtifact = "BotendoKotlin"
project.base.archivesName.set(mavenArtifact)

repositories {
    mavenCentral()
    maven("https://m2.dv8tion.net/releases")
    maven {
        name = "Gitlab"
        val projectId = System.getenv("CI_PROJECT_ID")
        val apiV4 = System.getenv("CI_API_V4_URL")
        url = uri("https://$apiV4/projects/$projectId/packages/maven")
        authentication {
            create("header", HttpHeaderAuthentication::class.java) {
                if (System.getenv("CI_JOB_TOKEN") != null) {
                    credentials(HttpHeaderCredentials::class) {
                        name = "Job-Token"
                        value = System.getenv("CI_JOB_TOKEN")
                    }
                } else {
                    credentials(HttpHeaderCredentials::class) {
                        name = "Private-Token"
                        value = project.ext["myGitlabToken"] as String
                    }
                }
            }
        }
    }
}

dependencies {
    implementation("net.dv8tion:JDA:5.0.0-beta.6")
    implementation("com.sedmelluq:lavaplayer:1.3.77")
    implementation("org.slf4j:slf4j-api:2.0.3")
    implementation("org.slf4j:slf4j-simple:2.0.3")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.1.0")
}


val targetJavaVersion = 17
val templateSrc = project.rootDir.resolve("src/main/templates")
val templateDest = project.buildDir.resolve("generated/templates")
val templateProps = mapOf(
    "version" to project.version as String,
    "ownerID" to ownerID
)



tasks {
    create<Copy>("generateTemplates") {
        filteringCharset = "UTF-8"

        inputs.properties(templateProps)
        from(templateSrc)
        expand(templateProps)
        into(templateDest)
    }

    withType<Jar> {
        manifest {
            attributes["Main-Class"] = "de.limited_dev.botendo.MainKt"
        }
        // To add all of the dependencies
        from(sourceSets.main.get().output)

        dependsOn(configurations.runtimeClasspath)
        from({
            configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
        })
        duplicatesStrategy = DuplicatesStrategy.INCLUDE
        dependsOn("generateTemplates", "processResources")
    }


    withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
        dependencies {
            include(dependency("net.dv8tion:JDA:5.0.0-beta.6"))
            include(dependency("com.sedmelluq:lavaplayer:1.3.77"))
            include(dependency("org.slf4j:slf4j-api:2.0.3"))
            include(dependency("org.slf4j:slf4j-simple:2.0.3"))
            include(dependency("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.1.0"))
        }
        dependsOn("generateTemplates", "processResources")
    }

    withType<JavaCompile> {
        // ensure that the encoding is set to UTF-8, no matter what the system default is
        // this fixes some edge cases with special characters not displaying correctly
        // see http://yodaconditions.net/blog/fix-for-java-file-encoding-problems-with-gradle.html
        // If Javadoc is generated, this must be specified in that task too.
        options.encoding = "UTF-8"
        if (targetJavaVersion >= 10 || JavaVersion.current().isJava10Compatible) {
            options.release.set(targetJavaVersion)
        }

        dependsOn("generateTemplates", "processResources")
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = targetJavaVersion.toString()

        dependsOn("generateTemplates", "processResources")
    }

    withType<Jar> {
        from("LICENSE") {
            rename { "${it}_${project.base.archivesName.get()}" }
        }

        archiveBaseName.set(mavenArtifact)

        dependsOn("generateTemplates", "processResources")
    }
}


java {
    val javaVersion = JavaVersion.toVersion(targetJavaVersion)
    if (JavaVersion.current() < javaVersion) {
        toolchain.languageVersion.set(JavaLanguageVersion.of(targetJavaVersion))
    }

    withSourcesJar()
}

sourceSets {
    main {
        java {
            srcDir(templateDest)
        }
    }
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            version = project.version as String
            artifactId = mavenArtifact
            from(components["java"])
        }
    }

    repositories {
        if (System.getenv("CI_JOB_TOKEN") != null) {
            maven {
                name = "GitLab"
                val projectId = System.getenv("CI_PROJECT_ID")
                val apiV4 = System.getenv("CI_API_V4_URL")
                url = uri("$apiV4/projects/$projectId/packages/maven")
                authentication {
                    create("token", HttpHeaderAuthentication::class.java) {
                        credentials(HttpHeaderCredentials::class.java) {
                            name = "Job-Token"
                            value = System.getenv("CI_JOB_TOKEN")
                        }
                    }
                }
            }
        }
    }
}

rootProject.idea.project {
    this as ExtensionAware
    configure<ProjectSettings> {
        this as ExtensionAware
        configure<TaskTriggersConfig> {
            afterSync(tasks["generateTemplates"], tasks["processResources"])
        }
    }
}

//rootProject.eclipse.synchronizationTasks("generateTemplates", "processResources")
//Fuck eclipse users amirite?
